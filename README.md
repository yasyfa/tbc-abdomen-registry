# ⚕️ [TBC Abdomen Registry](https://tbc-abdomen-registry.herokuapp.com/) ⚕️

TBC Abdomen Registry is a TBC Abdomen database, created using django, chartjs, and postgreSQL.<br/>
Designed, and Developed by 3 amazing people OxaGyne, HuuHeeHaa, AxeCriminal.<br/>
We hope this website helps doctor to save others life 🙌

## Features ✨

- Simplifiy registering patient using django forms
- Import and save data from/to .xlss file
- Present data using chartjs
