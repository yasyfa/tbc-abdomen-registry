dashboardChart = {
	initChart: function (id, api_address) {
		$.get(api_address, function (data, _) {
			if (Array.isArray(data.data.labels) && data.data.labels.length) {
				ctx = document.getElementById(id).getContext("2d");
				optionsPie = {
					tooltips: {
						callbacks: {
							label: function (tooltipItem, data) {
								//get the concerned dataset
								var dataset = data.datasets[tooltipItem.datasetIndex];
								//calculate the total of this data set
								var total = dataset.data.reduce(function (
									previousValue,
									currentValue,
									currentIndex,
									array
								) {
									return previousValue + currentValue;
								});
								//get the current items value
								var currentValue = dataset.data[tooltipItem.index];
								var label = data.labels[tooltipItem.index];
								//calculate the precentage based on the total and current item
								var percentage =
									Math.round((currentValue / total) * 10000) / 100;
								return label + ": " + currentValue + " (" + percentage + "%)";
							},
						},
					},
				};
				optionsBar = {
					scales: {
						yAxes: [
							{
								ticks: {
									beginAtZero: true,
								},
							},
						],
					},
				};
				if (data.type === "pie") {
					data.options = optionsPie;
				} else if (data.type === "bar") {
					data.options = optionsBar;
				}
				myChart = new Chart(ctx, data);
			} else {
				console.log("meng " + id);
				document.getElementById(id + "-warning").innerHTML =
					"Mohon maaf, saat ini data tidak tersedia.";
				document.getElementById(id).remove();
			}
		});
	},
};

$(document).ready(function () {
	dashboardChart.initChart("chart-jenis-kelamin", "data/jenis-kelamin");
	dashboardChart.initChart("chart-usia", "data/usia");
	dashboardChart.initChart("chart-output", "data/output");
	dashboardChart.initChart("chart-domisili", "data/domisili");
	dashboardChart.initChart("chart-kontak-tb", "data/kontak-tb");
	dashboardChart.initChart("chart-tst", "data/tst");
});