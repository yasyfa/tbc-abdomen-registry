from django.urls import path

from .views import dashboard_view, get_data_jenis_kelamin, get_data_usia, get_data_output, get_data_domisili, get_data_kontak_tb, get_data_tst

app_name = 'dashboard'

urlpatterns = [
    path('', dashboard_view, name='index'),

    path('data/jenis-kelamin/', get_data_jenis_kelamin, name='data-jenis-kelamin'),
    path('data/usia/', get_data_usia, name='data-usia'),
    path('data/output/', get_data_output, name='data-output'),
    path('data/domisili/', get_data_domisili, name='data-domisili'),
    path('data/kontak-tb/', get_data_kontak_tb, name='data-kontak-tb'),
    path('data/tst/', get_data_tst, name='data-tst'),
]
