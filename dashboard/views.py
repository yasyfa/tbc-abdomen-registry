from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db.models import Count

import datetime
import math
import random

from main.models.pasien import Pasien
from main.models.usia import Usia
from main.models.luaran_saat_rawat_inap import LuaranSaatRawatInap
from main.models.domisili import Domisili

@login_required(login_url='account:login')
def dashboard_view(request):
    return render(request, "dashboard/dashboard.html")

def dynamic_color():
    red = math.floor(random.randint(0, 255))
    green = math.floor(random.randint(0, 255))
    blue = math.floor(random.randint(0, 255))
    return "rgb(%d,%d,%d)" % (red, green, blue)

def get_data_jenis_kelamin(_):
    jenis_kelamin_count = Pasien.objects.all().values("jenis_kelamin").annotate(
        count=Count("jenis_kelamin")).order_by("jenis_kelamin")
    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [
                dict(Pasien.JENIS_KELAMIN_CHOICES).get(x["jenis_kelamin"],
                                                       x["jenis_kelamin"])
                for x in jenis_kelamin_count if x["jenis_kelamin"]
            ],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(jenis_kelamin_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in jenis_kelamin_count if x["jenis_kelamin"]],
            }]
        }
    })

def get_data_usia(_):
    usia_count = Usia.objects.all().values("kategori").annotate(
        count=Count("kategori")).order_by("kategori")
    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [
                dict(Usia.USIA_CHOICES).get(x["kategori"], x["kategori"])
                for x in usia_count if x["kategori"]
            ],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(usia_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in usia_count if x["kategori"]],
            }]
        }
    })

def get_data_output(_):
    hidup_count = 0
    meninggal_count = 0

    output = LuaranSaatRawatInap.objects.all()

    for data in output:
        if data.status  == "Pulang Paksa" or data.status == "Perbaikan":
            hidup_count += 1
        elif data.status  == "Meninggal" :
            meninggal_count += 1


    labels = ['Meninggal', 'Hidup'] if Pasien.objects.all().count() != 0 else []

    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": labels,
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(2)],
                "borderWidth":
                2,
                "data": [meninggal_count, hidup_count],
            }]
        }
    })

def get_data_domisili(_):
    domisili = Domisili.objects.all()

    bandung_count = 0   
    luar_bandung_count = 0
    for data in domisili:
        if data.bandung != None and data.bandung.strip() != '-' and data.bandung != "" and data.bandung != False :
            bandung_count += 1
        if data.luarBandung != None and data.luarBandung.strip() != '-' and data.luarBandung != "" and data.luarBandung != False:
            luar_bandung_count += 1
        
    

    domisili_count = [
        {
            'domisili': 'Bandung',
            'count': bandung_count
        },
        {
            'domisili': 'Luar Bandung',
            'count': luar_bandung_count
        },
    ]

    labels = ['Bandung', 'Luar Bandung'] if Pasien.objects.all().count() != 0 else []

    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": labels,
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(domisili_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in domisili_count if x["domisili"]],
            }]
        }
    })

def get_data_kontak_tb(_):
    kontak_tb_count = Pasien.objects.all().values("riwayat_kontak_tb_dewasa").annotate(
        count=Count("riwayat_kontak_tb_dewasa")).order_by("riwayat_kontak_tb_dewasa")
    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": [
                dict(Pasien.YA_TIDAK_CHOICES).get(x["riwayat_kontak_tb_dewasa"],
                                                       x["riwayat_kontak_tb_dewasa"])
                for x in kontak_tb_count if x["riwayat_kontak_tb_dewasa"]
            ],
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(kontak_tb_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in kontak_tb_count if x["riwayat_kontak_tb_dewasa"]],
            }]
        }
    })

def get_data_tst(_):
    tst_count = Pasien.objects.all().values("tst").annotate(
        count=Count("tst")).order_by("tst")

    labels = ['Dilakukan', 'Tidak Dilakukan'] if Pasien.objects.all().count() != 0 else []
    return JsonResponse({
        "type": "doughnut",
        "responsive": True,
        "maintainAspectRatio": False,
        "responsiveAnimationDuration": 250,
        "data": {
            "labels": labels,
            "datasets": [{
                "borderColor":
                "#1f8ef1",
                "backgroundColor":
                [dynamic_color() for _ in range(len(tst_count))],
                "borderWidth":
                2,
                "data":
                [x["count"] for x in tst_count if x["tst"]],
            }]
        }
    })