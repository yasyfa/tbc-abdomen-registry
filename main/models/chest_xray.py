from main.models import pembesaran_kgb
from django.db import models

class ChestXray(models.Model):
    XRAY_CHOICES = (("1", "1"), ("-", "-"))
    normal = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    klasifikasi_dengan_infiltat = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    pembesaran_kgb_hilus_atau_paratrakeal = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    kavitas = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    efusi_pleura_chest_xray = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    konsolidasi = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    milier = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    tuberkoloma = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    atelektasis = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=XRAY_CHOICES)
    ekspertise = models.TextField(max_length=255, 
                                    blank=True,
                                    null=True)


    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in ChestXray._meta.fields]

    def get_header():
        return [(field.name) for field in ChestXray._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in ChestXray._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in ChestXray._meta.fields}
