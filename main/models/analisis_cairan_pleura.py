from django.db import models

class AnalisisCairanPleura(models.Model):
    warna_pleura = models.CharField(max_length=255, blank=True, null=True)
    rivalta_pleura = models.CharField(max_length=255, blank=True, null=True)
    jumlah_sel_pleura = models.CharField(max_length=255, blank=True, null=True)
    pmn_pleura = models.CharField(max_length=255, blank=True, null=True)
    mn_pleura = models.CharField(max_length=255, blank=True, null=True)
    glukosa_pleura = models.CharField(max_length=255, blank=True, null=True)
    protein_pleura = models.CharField(max_length=255, blank=True, null=True)
    albumin_pleura = models.CharField(max_length=255, blank=True, null=True)
    ldh_pleura = models.CharField(max_length=255, blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in AnalisisCairanPleura._meta.fields]

    def get_header():
        return [(field.name) for field in AnalisisCairanPleura._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in AnalisisCairanPleura._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in AnalisisCairanPleura._meta.fields}