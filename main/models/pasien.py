from .suhu import Suhu
from .nyeri_perut import NyeriPerut
from .perut_membesar import PerutMembesar
from .status_gizi import StatusGizi
from .bta_sputum_aspirat_lambung import BtaSputumAspiratLambung
from .sesak_napas import SesakNapas
from .riwayat_batuk import RiwayatBatuk
from .riwayat_panas import RiwayatPanas
from .diare_kronis import DiareKronis
from .pembesaran_kgb import PembesaranKgb
from .pemeriksaan_abdomen import PemeriksaanAbdomen
from .pemeriksaan_toraks import PemeriksaanToraks
from .laboratorium import Laboratorium
from .usg_abdomen import UsgAbdomen
from .chest_xray import ChestXray
from .tcm_pleura import TcmPleura
from .domisili import Domisili
from .analisis_cairan_asites import AnalisisCairanAsites
from .analisis_cairan_pleura import AnalisisCairanPleura
from .luaran_saat_rawat_inap import LuaranSaatRawatInap
from .usia import Usia

from django.db import models

# Create your models here.


class Pasien(models.Model):
    JENIS_KELAMIN_CHOICES = (("Laki-laki", "Laki-laki"),
                             ("Perempuan", "Perempuan"))

    PENDIDIKAN_CHOICES = (("Tidak Bersekolah", "Tidak Bersekolah"),
                          ("SD", "SD"), ("SMP", "SMP"), ("SMA", "SMA"))

    YA_TIDAK_CHOICES = (("Ya", "Ya"),
                        ("Tidak", "Tidak"))

    DILAKUKAN_TIDAK_CHOICES = (("Dilakukan", "Dilakukan"),
                               ("Tidak Dilakukan", "Tidak Dilakukan"))

    POSITIF_NEGATIF_CHOICES = (("Positif", "Positif"),
                               ("Negatif", "Negatif"))

    POSITIF_NEGATIF_TIDAK_DEKATAHUI_CHOICES = (("Positif", "Positif"),
                                               ("Negatif", "Negatif"),
                                               ("Tidak diketahui", "Tidak diketahui"))

    MTB_CHOICES = (("MTB detected", "MTB detected"),
                   ("MTB Not detected", "MTB Not detected"),
                   ("Tidak diperiksakan", "Tidak diperiksakan"))

    EFEK_SAMPING_OAT_CHOICES = (("ADIH", "ADIH"),
                                ("Tidak ada", "Tidak ada"))

    rm = models.CharField(max_length=255, unique=True)

    tanggal_perawatan = models.DateField(null=True, blank=True)

    no_hp = models.CharField(max_length=255, blank=True, null=True)

    nama_pasien = models.CharField(max_length=255, blank=True, null=True)

    jenis_kelamin = models.CharField(max_length=255,
                                     blank=True,
                                     null=True,
                                     choices=JENIS_KELAMIN_CHOICES)

    rujukan_dari = models.CharField(max_length=255, blank=True, null=True)

    domisili = models.OneToOneField(
        Domisili, on_delete=models.CASCADE, null=True, blank=True)

    pendidikan = models.CharField(max_length=255,
                                  blank=True,
                                  null=True,
                                  choices=PENDIDIKAN_CHOICES)

    usia = models.OneToOneField(
        Usia, on_delete=models.CASCADE, null=True, blank=True)

    status_gizi = models.OneToOneField(
        StatusGizi, on_delete=models.CASCADE, null=True, blank=True)

    status_hiv = models.CharField(max_length=255,
                                  blank=True,
                                  null=True,
                                  choices=POSITIF_NEGATIF_TIDAK_DEKATAHUI_CHOICES)

    perut_membesar = models.OneToOneField(
        PerutMembesar, on_delete=models.CASCADE, null=True, blank=True)

    nyeri_perut = models.OneToOneField(
        NyeriPerut, on_delete=models.CASCADE, null=True, blank=True)

    keringat_malam = models.CharField(max_length=255,
                                      blank=True,
                                      null=True,
                                      choices=YA_TIDAK_CHOICES)

    sesak_napas = models.OneToOneField(
        SesakNapas, on_delete=models.CASCADE, null=True, blank=True)

    edema = models.CharField(max_length=255,
                             blank=True,
                             null=True,
                             choices=YA_TIDAK_CHOICES)

    muntah = models.CharField(max_length=255,
                              blank=True,
                              null=True,
                              choices=YA_TIDAK_CHOICES)

    anoreksia = models.CharField(max_length=255,
                                 blank=True,
                                 null=True,
                                 choices=YA_TIDAK_CHOICES)

    diare_kronis = models.OneToOneField(
        DiareKronis, on_delete=models.CASCADE, null=True, blank=True)

    hematoskezia = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)

    riwayat_panas = models.OneToOneField(
        RiwayatPanas, on_delete=models.CASCADE, null=True, blank=True)

    riwayat_batuk = models.OneToOneField(
        RiwayatBatuk, on_delete=models.CASCADE, null=True, blank=True)

    riwayat_bb_sulit_naik = models.CharField(max_length=255,
                                             blank=True,
                                             null=True,
                                             choices=YA_TIDAK_CHOICES)

    penurunan_bb = models.TextField(blank=True, null=True)

    malaise = models.CharField(max_length=255,
                               blank=True,
                               null=True,
                               choices=YA_TIDAK_CHOICES)

    riwayat_kontak_tb_dewasa = models.CharField(max_length=255,
                                                blank=True,
                                                null=True,
                                                choices=YA_TIDAK_CHOICES)

    siapa = models.TextField(blank=True, null=True)

    riwayat_imunisasi_bcg = models.CharField(max_length=255,
                                             blank=True,
                                             null=True,
                                             choices=YA_TIDAK_CHOICES)

    riwayat_pengobatan_tb = models.CharField(max_length=255,
                                             blank=True,
                                             null=True,
                                             choices=YA_TIDAK_CHOICES)

    kapan_pengobatan_tb_sebelumnya = models.TextField(blank=True, null=True)

    durasi_pengobatan_tb_sebelumnya = models.TextField(blank=True, null=True)

    keadaan_umum = models.TextField(blank=True, null=True)

    suhu = models.OneToOneField(
        Suhu, on_delete=models.CASCADE, null=True, blank=True)

    pembesaran_kgb = models.OneToOneField(
        PembesaranKgb, on_delete=models.CASCADE, null=True, blank=True)

    pemeriksaan_toraks = models.OneToOneField(
        PemeriksaanToraks, on_delete=models.CASCADE, null=True, blank=True)

    pemeriksaan_abdomen = models.OneToOneField(
        PemeriksaanAbdomen, on_delete=models.CASCADE, null=True, blank=True)

    efusi_pleura = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)

    efusi_perikardium = models.CharField(max_length=255,
                                         blank=True,
                                         null=True,
                                         choices=YA_TIDAK_CHOICES)

    laboratorium = models.OneToOneField(
        Laboratorium, on_delete=models.CASCADE, null=True, blank=True)

    tst = models.CharField(max_length=255,
                           blank=True,
                           null=True,
                           choices=DILAKUKAN_TIDAK_CHOICES)

    tst_mm = models.CharField(max_length=255,
                              blank=True,
                              null=True)

    usg_abdomen = models.OneToOneField(
        UsgAbdomen, on_delete=models.CASCADE, null=True, blank=True)

    chest_xray = models.OneToOneField(
        ChestXray, on_delete=models.CASCADE, null=True, blank=True)

    bta_sputum_aspirat_lambung = models.OneToOneField(
        BtaSputumAspiratLambung, on_delete=models.CASCADE, null=True, blank=True)

    tcm_sputum_aspirat_lambung = models.CharField(max_length=255,
                                                  blank=True,
                                                  null=True,
                                                  choices=MTB_CHOICES)

    tcm_asites = models.CharField(max_length=255,
                                  blank=True,
                                  null=True,
                                  choices=MTB_CHOICES)

    tcm_pleura = models.OneToOneField(
        TcmPleura, on_delete=models.CASCADE, null=True, blank=True)

    tcm_feses = models.CharField(max_length=255,
                                 blank=True,
                                 null=True,
                                 choices=MTB_CHOICES)

    kultur_mtb = models.TextField(blank=True, null=True)

    lcs = models.TextField(blank=True, null=True)

    fnab_pa_jaringan = models.TextField(blank=True, null=True)

    ct_scan_abdomen = models.TextField(blank=True, null=True)

    analisis_cairan_asites = models.OneToOneField(
        AnalisisCairanAsites, on_delete=models.CASCADE, null=True, blank=True)

    analisis_cairan_pleura = models.OneToOneField(
        AnalisisCairanPleura, on_delete=models.CASCADE, null=True, blank=True)

    diagnosis_akhir = models.TextField(blank=True, null=True)

    diagnosis_komorbiditas = models.TextField(blank=True, null=True)

    regimen_oat = models.TextField(blank=True, null=True)

    prednison = models.CharField(max_length=255,
                                 blank=True,
                                 null=True,
                                 choices=YA_TIDAK_CHOICES)

    efek_samping_oat = models.CharField(max_length=255,
                                        blank=True,
                                        null=True,
                                        choices=EFEK_SAMPING_OAT_CHOICES)

    regimen_oat_diganti_dengan = models.TextField(blank=True, null=True)

    los = models.CharField(max_length=255, blank=True, null=True)

    luaran_saat_rawat_inap = models.OneToOneField(
        LuaranSaatRawatInap, on_delete=models.CASCADE, null=True, blank=True)

    keterangan_lain = models.TextField(blank=True, null=True)

    dpjp_utama = models.TextField(blank=True, null=True)

    tanggal_mulai_oat = models.DateField(blank=True, null=True)

    status_gizi_1 = models.TextField(blank=True, null=True)
    keluhan_1 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_1 = models.TextField(blank=True, null=True)
    lab_1 = models.TextField(blank=True, null=True)
    radiologi_1 = models.TextField(blank=True, null=True)
    regimen_oat_1 = models.TextField(blank=True, null=True)
    diagnosis_1 = models.TextField(blank=True, null=True)
    komplikasi_1 = models.TextField(blank=True, null=True)
    lain_lain_1 = models.TextField(blank=True, null=True)

    status_gizi_2 = models.TextField(blank=True, null=True)
    keluhan_2 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_2 = models.TextField(blank=True, null=True)
    lab_2 = models.TextField(blank=True, null=True)
    radiologi_2 = models.TextField(blank=True, null=True)
    regimen_oat_2 = models.TextField(blank=True, null=True)
    diagnosis_2 = models.TextField(blank=True, null=True)
    komplikasi_2 = models.TextField(blank=True, null=True)
    lain_lain_2 = models.TextField(blank=True, null=True)

    status_gizi_3 = models.TextField(blank=True, null=True)
    keluhan_3 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_3 = models.TextField(blank=True, null=True)
    lab_3 = models.TextField(blank=True, null=True)
    radiologi_3 = models.TextField(blank=True, null=True)
    regimen_oat_3 = models.TextField(blank=True, null=True)
    diagnosis_3 = models.TextField(blank=True, null=True)
    komplikasi_3 = models.TextField(blank=True, null=True)
    lain_lain_3 = models.TextField(blank=True, null=True)

    status_gizi_4 = models.TextField(blank=True, null=True)
    keluhan_4 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_4 = models.TextField(blank=True, null=True)
    lab_4 = models.TextField(blank=True, null=True)
    radiologi_4 = models.TextField(blank=True, null=True)
    regimen_oat_4 = models.TextField(blank=True, null=True)
    diagnosis_4 = models.TextField(blank=True, null=True)
    komplikasi_4 = models.TextField(blank=True, null=True)
    lain_lain_4 = models.TextField(blank=True, null=True)

    status_gizi_5 = models.TextField(blank=True, null=True)
    keluhan_5 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_5 = models.TextField(blank=True, null=True)
    lab_5 = models.TextField(blank=True, null=True)
    radiologi_5 = models.TextField(blank=True, null=True)
    regimen_oat_5 = models.TextField(blank=True, null=True)
    diagnosis_5 = models.TextField(blank=True, null=True)
    komplikasi_5 = models.TextField(blank=True, null=True)
    lain_lain_5 = models.TextField(blank=True, null=True)
    
    status_gizi_6 = models.TextField(blank=True, null=True)
    keluhan_6 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_6 = models.TextField(blank=True, null=True)
    lab_6 = models.TextField(blank=True, null=True)
    radiologi_6 = models.TextField(blank=True, null=True)
    regimen_oat_6 = models.TextField(blank=True, null=True)
    diagnosis_6 = models.TextField(blank=True, null=True)
    komplikasi_6 = models.TextField(blank=True, null=True)
    lain_lain_6 = models.TextField(blank=True, null=True)

    status_gizi_7 = models.TextField(blank=True, null=True)
    keluhan_7 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_7 = models.TextField(blank=True, null=True)
    lab_7 = models.TextField(blank=True, null=True)
    radiologi_7 = models.TextField(blank=True, null=True)
    regimen_oat_7 = models.TextField(blank=True, null=True)
    diagnosis_7 = models.TextField(blank=True, null=True)
    komplikasi_7 = models.TextField(blank=True, null=True)
    lain_lain_7 = models.TextField(blank=True, null=True)

    status_gizi_8 = models.TextField(blank=True, null=True)
    keluhan_8 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_8 = models.TextField(blank=True, null=True)
    lab_8 = models.TextField(blank=True, null=True)
    radiologi_8 = models.TextField(blank=True, null=True)
    regimen_oat_8 = models.TextField(blank=True, null=True)
    diagnosis_8 = models.TextField(blank=True, null=True)
    komplikasi_8 = models.TextField(blank=True, null=True)
    lain_lain_8 = models.TextField(blank=True, null=True)

    status_gizi_9 = models.TextField(blank=True, null=True)
    keluhan_9 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_9 = models.TextField(blank=True, null=True)
    lab_9 = models.TextField(blank=True, null=True)
    radiologi_9 = models.TextField(blank=True, null=True)
    regimen_oat_9 = models.TextField(blank=True, null=True)
    diagnosis_9 = models.TextField(blank=True, null=True)
    komplikasi_9 = models.TextField(blank=True, null=True)
    lain_lain_9 = models.TextField(blank=True, null=True)

    status_gizi_10 = models.TextField(blank=True, null=True)
    keluhan_10 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_10 = models.TextField(blank=True, null=True)
    lab_10 = models.TextField(blank=True, null=True)
    radiologi_10 = models.TextField(blank=True, null=True)
    regimen_oat_10 = models.TextField(blank=True, null=True)
    diagnosis_10 = models.TextField(blank=True, null=True)
    komplikasi_10 = models.TextField(blank=True, null=True)
    lain_lain_10 = models.TextField(blank=True, null=True)

    status_gizi_11 = models.TextField(blank=True, null=True)
    keluhan_11 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_11 = models.TextField(blank=True, null=True)
    lab_11 = models.TextField(blank=True, null=True)
    radiologi_11 = models.TextField(blank=True, null=True)
    regimen_oat_11 = models.TextField(blank=True, null=True)
    diagnosis_11 = models.TextField(blank=True, null=True)
    komplikasi_11 = models.TextField(blank=True, null=True)
    lain_lain_11 = models.TextField(blank=True, null=True)

    status_gizi_12 = models.TextField(blank=True, null=True)
    keluhan_12 = models.TextField(blank=True, null=True)
    pemeriksaan_fisik_12 = models.TextField(blank=True, null=True)
    lab_12 = models.TextField(blank=True, null=True)
    radiologi_12 = models.TextField(blank=True, null=True)
    regimen_oat_12 = models.TextField(blank=True, null=True)
    diagnosis_12 = models.TextField(blank=True, null=True)
    komplikasi_12 = models.TextField(blank=True, null=True)
    lain_lain_12 = models.TextField(blank=True, null=True)

    durasi_oat = models.TextField(blank=True, null=True)
    hasil_akhir_pengobatan = models.TextField(blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Pasien._meta.fields]

    def get_header():
        return [(field.name) for field in Pasien._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Pasien._meta.fields]

    def dict_value(self):
        return {field.name: field.value_from_object(self) for field in Pasien._meta.fields}

    def delete(self):
        if self.domisili:
            self.domisili.delete()
        if self.analisis_cairan_asites:
            self.analisis_cairan_asites.delete()
        if self.analisis_cairan_pleura:
            self.analisis_cairan_pleura.delete()
        if self.bta_sputum_aspirat_lambung:
            self.bta_sputum_aspirat_lambung.delete()
        if self.chest_xray:
            self.chest_xray.delete()
        if self.diare_kronis:
            self.diare_kronis.delete()
        if self.laboratorium:
            self.laboratorium.delete()
        if self.luaran_saat_rawat_inap:
            self.luaran_saat_rawat_inap.delete()
        if self.nyeri_perut:
            self.nyeri_perut.delete()
        if self.pembesaran_kgb:
            self.pembesaran_kgb.delete()
        if self.pemeriksaan_abdomen:
            self.pemeriksaan_abdomen.delete()
        if self.pemeriksaan_toraks:
            self.pemeriksaan_toraks.delete()
        if self.perut_membesar:
            self.perut_membesar.delete()
        if self.riwayat_batuk:
            self.riwayat_batuk.delete()
        if self.riwayat_panas:
            self.riwayat_panas.delete()
        if self.sesak_napas:
            self.sesak_napas.delete()
        if self.status_gizi:
            self.status_gizi.delete()
        if self.suhu:
            self.suhu.delete()
        if self.usia:
            self.usia.delete()
        if self.tcm_pleura:
            self.tcm_pleura.delete()
        if self.usg_abdomen:
            self.usg_abdomen.delete()

    def __str__(self):
        return self.rm
