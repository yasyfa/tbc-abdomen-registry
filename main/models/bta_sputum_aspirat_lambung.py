from django.db import models

class BtaSputumAspiratLambung(models.Model):
    POSITIF_NEGATIF_CHOICES = (("Positif", "Positif"), 
                                ("Negatif", "Negatif"))
    bta_1 = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=POSITIF_NEGATIF_CHOICES)
    bta_2 = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=POSITIF_NEGATIF_CHOICES)
    bta_3 = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=POSITIF_NEGATIF_CHOICES)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in BtaSputumAspiratLambung._meta.fields]

    def get_header():
        return [(field.name) for field in BtaSputumAspiratLambung._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in BtaSputumAspiratLambung._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in BtaSputumAspiratLambung._meta.fields}