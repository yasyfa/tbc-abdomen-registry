from django.db import models

class LuaranSaatRawatInap(models.Model):
    LUARAN_CHOICE = (("Pulang Paksa", "Pulang Paksa"), 
    ("Perbaikan", "Perbaikan"), 
    ("Meninggal", "Meninggal"))

    status = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=LUARAN_CHOICE)
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in LuaranSaatRawatInap._meta.fields]

    def get_header():
        return [(field.name) for field in LuaranSaatRawatInap._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in LuaranSaatRawatInap._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in LuaranSaatRawatInap._meta.fields}
                                    
