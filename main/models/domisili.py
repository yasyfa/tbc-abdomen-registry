from django.db import models

class Domisili(models.Model):
    bandung = models.TextField(blank=True,null=True)
    luarBandung = models.TextField(blank=True,null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Domisili._meta.fields]

    def get_header():
        return [(field.name) for field in Domisili._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Domisili._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in Domisili._meta.fields}
