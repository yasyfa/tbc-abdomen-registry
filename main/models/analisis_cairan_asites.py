from django.db import models

class AnalisisCairanAsites(models.Model):
    warna_asites = models.CharField(max_length=255, blank=True, null=True)
    rivalta_asites = models.CharField(max_length=255, blank=True, null=True)
    jumlah_sel_asites = models.CharField(max_length=255, blank=True, null=True)
    pmn_asites = models.CharField(max_length=255, blank=True, null=True)
    mn_asites = models.CharField(max_length=255, blank=True, null=True)
    glukosa_asites = models.CharField(max_length=255, blank=True, null=True)
    albumin_asites = models.CharField(max_length=255, blank=True, null=True)
    protein_asites = models.CharField(max_length=255, blank=True, null=True)
    ldh_asites = models.CharField(max_length=255, blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in AnalisisCairanAsites._meta.fields]

    def get_header():
        return [(field.name) for field in AnalisisCairanAsites._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in AnalisisCairanAsites._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in AnalisisCairanAsites._meta.fields}