from django.db import models

class Usia (models.Model):
    USIA_CHOICES = (('< 5 Tahun', '< 5 Tahun'),
                    ('5-9 Tahun', '5-9 Tahun'),
                    ('10-14 Tahun', '10-14 Tahun'),
                    ('15-17 Tahun', '15-17 Tahun'))

    usia = models.CharField(blank=True, null=True, max_length=255)
    kategori = models.CharField(blank=True, null=True, max_length=255, choices=USIA_CHOICES)
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Usia._meta.fields]

    def get_header():
        return [(field.name) for field in Usia._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Usia._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in Usia._meta.fields}