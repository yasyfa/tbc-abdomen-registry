from django.db import models

class PembesaranKgb(models.Model):
    YA_TIDAK_CHOICES = (("Ya", "Ya"), ("Tidak", "Tidak"))
    pembesaran_kgb_utama = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)
    lokasi = models.TextField(blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in PembesaranKgb._meta.fields]

    def get_header():
        return [(field.name) for field in PembesaranKgb._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in PembesaranKgb._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in PembesaranKgb._meta.fields}