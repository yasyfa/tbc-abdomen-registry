from django.db import models

class Laboratorium(models.Model):
    hb = models.CharField(max_length=255, blank=True, null=True)
    ht = models.CharField(max_length=255, blank=True, null=True)
    leukosit = models.CharField(max_length=255, blank=True, null=True)
    trombosit = models.CharField(max_length=255, blank=True, null=True)
    basofil = models.CharField(max_length=255, blank=True, null=True)
    eosinofil = models.CharField(max_length=255, blank=True, null=True)
    n_batang = models.CharField(max_length=255, blank=True, null=True)
    n_segemen = models.CharField(max_length=255, blank=True, null=True)
    limfosit = models.CharField(max_length=255, blank=True, null=True)
    monosit = models.CharField(max_length=255, blank=True, null=True)
    sgot = models.CharField(max_length=255, blank=True, null=True)
    sgpt = models.CharField(max_length=255, blank=True, null=True)
    bil_total = models.CharField(max_length=255, blank=True, null=True)
    bil_direk = models.CharField(max_length=255, blank=True, null=True)
    albumin = models.CharField(max_length=255, blank=True, null=True)
    crp = models.CharField(max_length=255, blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Laboratorium._meta.fields]

    def get_header():
        return [(field.name) for field in Laboratorium._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Laboratorium._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in Laboratorium._meta.fields}
