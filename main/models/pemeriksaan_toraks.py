from django.db import models

class PemeriksaanToraks(models.Model):
    PEMERIKASAAN_TORAKS_CHOICES = (("Normal", "Normal"),
                             ("Abnormal", "Abnormal"))
    
    pemeriksaan_toraks = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=PEMERIKASAAN_TORAKS_CHOICES)

    deskripsi = models.TextField(blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in PemeriksaanToraks._meta.fields]

    def get_header():
        return [(field.name) for field in PemeriksaanToraks._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in PemeriksaanToraks._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in PemeriksaanToraks._meta.fields}