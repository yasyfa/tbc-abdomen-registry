from django.db import models

class TcmPleura(models.Model):
    MTB_CHOICES = (("MTB detected", "MTB detected"), 
                    ("MTB Not detected", "MTB Not detected"),
                    ("Tidak diperiksakan", "Tidak diperiksakan"))
    tcm_pleura = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=MTB_CHOICES)

    keterangan_tcm_pleura = models.TextField(blank=True,
                                    null=True,)
                                    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in TcmPleura._meta.fields]

    def get_header():
        return [(field.name) for field in TcmPleura._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in TcmPleura._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in TcmPleura._meta.fields}