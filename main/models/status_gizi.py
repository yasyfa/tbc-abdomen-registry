from django.db import models

class StatusGizi(models.Model):
    STATUS_CHOICES = (("Normal", "Normal"),("Malnutrisi Sedang", "Malnutrisi Sedang"), ("Malnutrisi Berat","Malnutrisi Berat"))
    status_gizi = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=STATUS_CHOICES)
    BB = models.CharField(max_length=255, blank=True, null=True)
    TB = models.CharField(max_length=255, blank=True, null=True)
    LLA = models.CharField(max_length=255, blank=True, null=True)
    TBU = models.CharField(max_length=255, blank=True, null=True)
    BMIU = models.CharField(max_length=255, blank=True, null=True)   
    BBTB = models.CharField(max_length=255, blank=True, null=True)
    LLAU = models.CharField(max_length=255, blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in StatusGizi._meta.fields]

    def get_header():
        return [(field.name) for field in StatusGizi._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in StatusGizi._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in StatusGizi._meta.fields}
    



    
    
    
