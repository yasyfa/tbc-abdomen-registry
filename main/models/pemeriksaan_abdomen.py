from django.db import models

class PemeriksaanAbdomen(models.Model):
    YA_TIDAK_CHOICES = (("Ya", "Ya"), ("Tidak", "Tidak"))

    distensi_abdomen = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)

    asites = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)

    hepatomegali = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)

    hepatomegali_keterangan = models.TextField(blank=True, null=True)    

    splenomegali = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)   
    
    splenomegali_keterangan = models.TextField(blank=True, null=True)  

    lingkar_perut_masuk = models.CharField(max_length=255,
                                    blank=True,
                                    null=True) 

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in PemeriksaanAbdomen._meta.fields]

    def get_header():
        return [(field.name) for field in PemeriksaanAbdomen._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in PemeriksaanAbdomen._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in PemeriksaanAbdomen._meta.fields}

