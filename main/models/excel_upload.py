from django.db import models

# Create your models here.

class Pasien(models.Model):
    JENIS_KELAMIN_CHOICES = (("Laki-laki", "Laki-laki"),
                             ("Perempuan", "Perempuan"))

    rm = models.CharField(max_length=255,primary_key=True)
    tanggalPerawatan = models.DateTimeField(null=True, blank=True)
    noHp = models.CharField(max_length=255, blank=True, null=True)
    namaPasien = models.CharField(max_length=255, blank=True, null=True)
    jenis_kelamin = models.CharField(max_length=255,
                                    blank=True,
                                    null=True,
                                    choices=JENIS_KELAMIN_CHOICES)


                                    

