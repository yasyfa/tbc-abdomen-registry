from django.db import models

class NyeriPerut(models.Model):
    YA_TIDAK_CHOICES = (("Ya", "Ya"), ("Tidak", "Tidak"))
    nyeriPerut = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=YA_TIDAK_CHOICES)
    keterangan_nyeri_perut = models.TextField(blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in NyeriPerut._meta.fields]

    def get_header():
        return [(field.name) for field in NyeriPerut._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in NyeriPerut._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in NyeriPerut._meta.fields}