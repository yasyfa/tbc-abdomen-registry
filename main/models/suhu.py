from django.db import models

class Suhu (models.Model):
    febris = models.CharField(blank=True, null=True, max_length=255)
    suhu = models.CharField(blank=True, null=True, max_length=255)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Suhu._meta.fields]

    def get_header():
        return [(field.name) for field in Suhu._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in Suhu._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in Suhu._meta.fields}