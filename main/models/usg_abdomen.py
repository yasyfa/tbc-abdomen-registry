from main.models import pembesaran_kgb
from django.db import models

class UsgAbdomen(models.Model):
    USG_CHOICES = (("1", "1"), ("-", "-"),("Tidak Dilakukan", "Tidak Dilakukan"))
    pembesaran_kgb = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=USG_CHOICES)
    asites_usg_abdomen = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=USG_CHOICES)
    hepatosplenomegali = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True,
                                    choices=USG_CHOICES)
    lain_lain = models.CharField(max_length=255, 
                                    blank=True,
                                    null=True)
    ekspertise = models.TextField(blank=True, null=True)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in UsgAbdomen._meta.fields]

    def get_header():
        return [(field.name) for field in UsgAbdomen._meta.fields]

    def get_all_value(self):
        return [field.value_from_object(self) for field in UsgAbdomen._meta.fields]

    def dict_value(self): 
        return {field.name : field.value_from_object(self) for field in UsgAbdomen._meta.fields}