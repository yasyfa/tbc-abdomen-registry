from main.models.pemeriksaan_toraks import PemeriksaanToraks
from main.models.pembesaran_kgb import PembesaranKgb
from main.models.domisili import Domisili
from main.models.status_gizi import StatusGizi
from .models.pasien import Pasien
from django.contrib import admin
from .models.usia import Usia
from .models.status_gizi import StatusGizi
from .models.pemeriksaan_abdomen import PemeriksaanAbdomen
from .models.bta_sputum_aspirat_lambung import BtaSputumAspiratLambung
from .models.luaran_saat_rawat_inap import LuaranSaatRawatInap
from .models import *

# Register your models here.

admin.site.register(Pasien)
admin.site.register(Domisili)
admin.site.register(Usia)
admin.site.register(PembesaranKgb)
admin.site.register(PemeriksaanToraks)
admin.site.register(PemeriksaanAbdomen)
admin.site.register(BtaSputumAspiratLambung)
admin.site.register(LuaranSaatRawatInap)