from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('list-pasien/', views.list_pasien, name='list_pasien'),
    path('form-pasien/', views.form_pasien, name='form_pasien'),
    path('export-excel/', views.download, name='export'),
    path('update/<str:rm_pasien>', views.update_pasien, name='edit_pasien'),
    path('delete/<str:rm_pasien>', views.delete_pasien, name='delete_pasien'),
]
