from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.http import HttpResponseNotAllowed, HttpResponse
from django.contrib.auth.decorators import login_required
from openpyxl.utils.datetime import to_excel

from .utils import getHeader
from datetime import datetime, date, timezone


from .forms.pasien_form import CreatePasienForm, ExcelUploadForm
from .forms.analisis_cairan_asites_form import AnalisisCairanAsitesForm
from .forms.analisis_cairan_pleura_form import AnalisisCairanPleuraForm
from .forms.suhu_form import SuhuForm
from .forms.usia_form import UsiaForm
from .forms.domisili_form import DomisiliForm
from .forms.chest_xray_form import ChestXrayForm
from .forms.tcm_pleura_form import TcmPleuraForm
from .forms.nyeri_perut_form import NyeriPerutForm
from .forms.sesak_napas_form import SesakNapasForm
from .forms.status_gizi_form import StatusGiziForm
from .forms.usg_abdomen_form import UsgAbdomenForm
from .forms.diare_kronis_form import DiareKronisForm
from .forms.laboratorium_form import LaboratoriumForm
from .forms.riwayat_batuk_form import RiwayatBatukForm
from .forms.riwayat_panas_form import RiwayatPanasForm
from .forms.pembesaran_kgb_form import PembesaranKgbForm
from .forms.perut_membesar_form import PerutMembesarForm
from .forms.pemeriksaan_toraks_form import PemeriksaanToraksForm
from .forms.luaran_saat_rawat_inap_form import LuaranSaatRawatInapForm
from .forms.pemeriksaan_abdomen_form import PemeriksaanAbdomenForm
from .forms.bta_sputum_aspirat_lambung import BtaSputumAspiratLambungForm

from .models.pasien import Pasien
from .models.analisis_cairan_asites import AnalisisCairanAsites
from .models.analisis_cairan_pleura import AnalisisCairanPleura
from .models.bta_sputum_aspirat_lambung import BtaSputumAspiratLambung
from .models.chest_xray import ChestXray
from .models.diare_kronis import DiareKronis
from .models.domisili import Domisili
from .models.laboratorium import Laboratorium
from .models.luaran_saat_rawat_inap import LuaranSaatRawatInap
from .models.nyeri_perut import NyeriPerut
from .models.pembesaran_kgb import PembesaranKgb
from .models.pemeriksaan_abdomen import PemeriksaanAbdomen
from .models.pemeriksaan_toraks import PemeriksaanToraks
from .models.perut_membesar import PerutMembesar
from .models.riwayat_batuk import RiwayatBatuk
from .models.riwayat_panas import RiwayatPanas
from .models.sesak_napas import SesakNapas
from .models.status_gizi import StatusGizi
from .models.suhu import Suhu
from .models.tcm_pleura import TcmPleura
from .models.usg_abdomen import UsgAbdomen
from .models.usia import Usia


from openpyxl import load_workbook, Workbook
from openpyxl.utils import get_column_letter
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Border, Side, Alignment, PatternFill

import copy
import datetime as dt


@login_required(login_url='account:login')
def home(request):

    return redirect('dashboard:index')


@login_required(login_url='account:login')
def form_pasien(request):
    pasien_form = CreatePasienForm(request.POST or None)
    domisili_form = DomisiliForm(request.POST or None)
    usia_form = UsiaForm(request.POST or None)
    status_gizi_form = StatusGiziForm(request.POST or None)
    perut_membesar_form = PerutMembesarForm(request.POST or None)
    nyeri_perut_form = NyeriPerutForm(request.POST or None)
    chest_xray_form = ChestXrayForm(request.POST or None)
    analisis_asites_form = AnalisisCairanAsitesForm(request.POST or None)
    analisis_pleura_form = AnalisisCairanPleuraForm(request.POST or None)
    suhu_form = SuhuForm(request.POST or None)
    tcm_pleura_form = TcmPleuraForm(request.POST or None)
    sesak_napas_form = SesakNapasForm(request.POST or None)
    usg_abdomen_form = UsgAbdomenForm(request.POST or None)
    pemeriksaan_abdomen_form = PemeriksaanAbdomenForm(request.POST or None)
    diare_kronis_form = DiareKronisForm(request.POST or None)
    laboratorium_form = LaboratoriumForm(request.POST or None)
    riwayat_batuk_form = RiwayatBatukForm(request.POST or None)
    riwayat_panas_form = RiwayatPanasForm(request.POST or None)
    pembesaran_kgb_form = PembesaranKgbForm(request.POST or None)
    pemeriksaan_toraks_form = PemeriksaanToraksForm(request.POST or None)
    luaran_saat_rawat_inap_form = LuaranSaatRawatInapForm(request.POST or None)
    bta_sputum_aspirat_lambung_form = BtaSputumAspiratLambungForm(
        request.POST or None)

    if request.method == 'POST':

        if pasien_form.is_valid():
            domisili = domisili_form.save()
            usia = usia_form.save()
            status_gizi = status_gizi_form.save()
            perut_membesar = perut_membesar_form.save()
            nyeri_perut = nyeri_perut_form.save()
            chest_xray = chest_xray_form.save()
            analisis_cairan_asites = analisis_asites_form.save()
            analisis_cairan_pleura = analisis_pleura_form.save()
            suhu = suhu_form.save()
            tcm_pleura = tcm_pleura_form.save()
            sesak_napas = sesak_napas_form.save()
            usg_abdomen = usg_abdomen_form.save()
            pemeriksaan_abdomen = pemeriksaan_abdomen_form.save()
            diare_kronis = diare_kronis_form.save()
            laboratorium = laboratorium_form.save()
            riwayat_batuk = riwayat_batuk_form.save()
            riwayat_panas = riwayat_panas_form.save()
            pembesaran_kgb = pembesaran_kgb_form.save()
            pemeriksaan_toraks = pemeriksaan_toraks_form.save()
            luaran_saat_rawat_inap = luaran_saat_rawat_inap_form.save()
            bta_sputum_aspirat_lambung = bta_sputum_aspirat_lambung_form.save()
            pasien = pasien_form.save()

            pasien.domisili = domisili
            pasien.usia = usia
            pasien.status_gizi = status_gizi
            pasien.perut_membesar = perut_membesar
            pasien.nyeri_perut = nyeri_perut
            pasien.chest_xray = chest_xray
            pasien.analisis_cairan_asites = analisis_cairan_asites
            pasien.analisis_cairan_pleura = analisis_cairan_pleura
            pasien.suhu = suhu
            pasien.tcm_pleura = tcm_pleura
            pasien.sesak_napas = sesak_napas
            pasien.usg_abdomen = usg_abdomen
            pasien.pemeriksaan_abdomen = pemeriksaan_abdomen
            pasien.diare_kronis = diare_kronis
            pasien.laboratorium = laboratorium
            pasien.riwayat_batuk = riwayat_batuk
            pasien.riwayat_panas = riwayat_panas
            pasien.pembesaran_kgb = pembesaran_kgb
            pasien.pemeriksaan_toraks = pemeriksaan_toraks
            pasien.luaran_saat_rawat_inap = luaran_saat_rawat_inap
            pasien.bta_sputum_aspirat_lambung = bta_sputum_aspirat_lambung
            pasien.save()
            return redirect('main:list_pasien')

    context = {
        'pasien_form': pasien_form,
        'usia_form': usia_form,
        'domisili_form': domisili_form,
        'status_gizi_form': status_gizi_form,
        'perut_membesar_form': perut_membesar_form,
        'nyeri_perut_form': nyeri_perut_form,
        'chest_xray_form': chest_xray_form,
        'analisis_asites_form': analisis_asites_form,
        'analisis_pleura_form': analisis_pleura_form,
        'suhu_form': suhu_form,
        'tcm_pleura_form': tcm_pleura_form,
        'sesak_napas_form': sesak_napas_form,
        'usg_abdomen_form': usg_abdomen_form,
        'diare_kronis_form': diare_kronis_form,
        'laboratorium_form': laboratorium_form,
        'riwayat_batuk_form': riwayat_batuk_form,
        'riwayat_panas_form': riwayat_panas_form,
        'pembesaran_kgb_form': pembesaran_kgb_form,
        'pemeriksaan_toraks_form': pemeriksaan_toraks_form,
        'luaran_saat_rawat_inap_form': luaran_saat_rawat_inap_form,
        'pemeriksaan_abdomen_form': pemeriksaan_abdomen_form,
        'bta_sputum_aspirat_lambung_form': bta_sputum_aspirat_lambung_form
    }

    return render(request, 'main/form-pasien.html', context=context)


@login_required(login_url='account:login')
def update_pasien(request, rm_pasien):
    pasien = Pasien.objects.get(rm=rm_pasien)
    data = pasien.dict_value()

    pasien_form = CreatePasienForm(
        request.POST or None, initial=data, instance=pasien)
    domisili_form = DomisiliForm(
        request.POST or None, initial=pasien.domisili.dict_value(), instance=pasien.domisili)
    usia_form = UsiaForm(request.POST or None,
                         initial=pasien.usia.dict_value(), instance=pasien.usia)
    status_gizi_form = StatusGiziForm(
        request.POST or None, instance=pasien.status_gizi, initial=pasien.status_gizi.dict_value())
    perut_membesar_form = PerutMembesarForm(
        request.POST or None, instance=pasien.perut_membesar, initial=pasien.perut_membesar.dict_value())
    nyeri_perut_form = NyeriPerutForm(
        request.POST or None, instance=pasien.nyeri_perut, initial=pasien.perut_membesar.dict_value())
    chest_xray_form = ChestXrayForm(
        request.POST or None, instance=pasien.chest_xray, initial=pasien.chest_xray.dict_value())
    analisis_asites_form = AnalisisCairanAsitesForm(
        request.POST or None, instance=pasien.analisis_cairan_asites, initial=pasien.analisis_cairan_asites.dict_value())
    analisis_pleura_form = AnalisisCairanPleuraForm(
        request.POST or None, instance=pasien.analisis_cairan_pleura, initial=pasien.analisis_cairan_pleura.dict_value())
    suhu_form = SuhuForm(request.POST or None,
                         initial=pasien.suhu.dict_value(), instance=pasien.suhu)
    tcm_pleura_form = TcmPleuraForm(
        request.POST or None, initial=pasien.tcm_pleura.dict_value(), instance=pasien.tcm_pleura)
    sesak_napas_form = SesakNapasForm(
        request.POST or None, initial=pasien.sesak_napas.dict_value(), instance=pasien.sesak_napas)
    usg_abdomen_form = UsgAbdomenForm(
        request.POST or None, initial=pasien.usg_abdomen.dict_value(), instance=pasien.usg_abdomen)
    pemeriksaan_abdomen_form = PemeriksaanAbdomenForm(
        request.POST or None, initial=pasien.pemeriksaan_abdomen.dict_value(), instance=pasien.pemeriksaan_abdomen)
    diare_kronis_form = DiareKronisForm(
        request.POST or None, initial=pasien.diare_kronis.dict_value(), instance=pasien.diare_kronis)
    laboratorium_form = LaboratoriumForm(
        request.POST or None, initial=pasien.laboratorium.dict_value(), instance=pasien.laboratorium)
    riwayat_batuk_form = RiwayatBatukForm(
        request.POST or None, initial=pasien.riwayat_batuk.dict_value(), instance=pasien.riwayat_batuk)
    riwayat_panas_form = RiwayatPanasForm(
        request.POST or None, initial=pasien.riwayat_panas.dict_value(), instance=pasien.riwayat_panas)
    pembesaran_kgb_form = PembesaranKgbForm(
        request.POST or None, initial=pasien.pembesaran_kgb.dict_value(), instance=pasien.pembesaran_kgb)
    pemeriksaan_toraks_form = PemeriksaanToraksForm(
        request.POST or None, initial=pasien.pemeriksaan_toraks.dict_value(), instance=pasien.pemeriksaan_toraks)
    luaran_saat_rawat_inap_form = LuaranSaatRawatInapForm(
        request.POST or None, initial=pasien.luaran_saat_rawat_inap.dict_value(), instance=pasien.luaran_saat_rawat_inap)
    bta_sputum_aspirat_lambung_form = BtaSputumAspiratLambungForm(
        request.POST or None, initial=pasien.bta_sputum_aspirat_lambung.dict_value(), instance=pasien.bta_sputum_aspirat_lambung)

    if request.method == 'POST':
        if pasien_form.is_valid and pemeriksaan_abdomen_form.is_valid:
            domisili = domisili_form.save()
            usia = usia_form.save()
            status_gizi = status_gizi_form.save()
            perut_membesar = perut_membesar_form.save()
            nyeri_perut = nyeri_perut_form.save()
            chest_xray = chest_xray_form.save()
            analisis_cairan_asites = analisis_asites_form.save()
            analisis_cairan_pleura = analisis_pleura_form.save()
            suhu = suhu_form.save()
            tcm_pleura = tcm_pleura_form.save()
            sesak_napas = sesak_napas_form.save()
            usg_abdomen = usg_abdomen_form.save()
            pemeriksaan_abdomen = pemeriksaan_abdomen_form.save()
            diare_kronis = diare_kronis_form.save()
            laboratorium = laboratorium_form.save()
            riwayat_batuk = riwayat_batuk_form.save()
            riwayat_panas = riwayat_panas_form.save()
            pembesaran_kgb = pembesaran_kgb_form.save()
            pemeriksaan_toraks = pemeriksaan_toraks_form.save()
            luaran_saat_rawat_inap = luaran_saat_rawat_inap_form.save()
            bta_sputum_aspirat_lambung = bta_sputum_aspirat_lambung_form.save()
            pasien = pasien_form.save()

            pasien.domisili = domisili
            pasien.usia = usia
            pasien.status_gizi = status_gizi
            pasien.perut_membesar = perut_membesar
            pasien.nyeri_perut = nyeri_perut
            pasien.chest_xray = chest_xray
            pasien.analisis_cairan_asites = analisis_cairan_asites
            pasien.analisis_cairan_pleura = analisis_cairan_pleura
            pasien.suhu = suhu
            pasien.tcm_pleura = tcm_pleura
            pasien.sesak_napas = sesak_napas
            pasien.usg_abdomen = usg_abdomen
            pasien.pemeriksaan_abdomen = pemeriksaan_abdomen
            pasien.diare_kronis = diare_kronis
            pasien.laboratorium = laboratorium
            pasien.riwayat_batuk = riwayat_batuk
            pasien.riwayat_panas = riwayat_panas
            pasien.pembesaran_kgb = pembesaran_kgb
            pasien.pemeriksaan_toraks = pemeriksaan_toraks
            pasien.luaran_saat_rawat_inap = luaran_saat_rawat_inap
            pasien.bta_sputum_aspirat_lambung = bta_sputum_aspirat_lambung
            pasien.save()

            return redirect('main:list_pasien')

    context = {
        'pasien_form': pasien_form,
        'usia_form': usia_form,
        'domisili_form': domisili_form,
        'status_gizi_form': status_gizi_form,
        'perut_membesar_form': perut_membesar_form,
        'nyeri_perut_form': nyeri_perut_form,
        'chest_xray_form': chest_xray_form,
        'analisis_asites_form': analisis_asites_form,
        'analisis_pleura_form': analisis_pleura_form,
        'suhu_form': suhu_form,
        'tcm_pleura_form': tcm_pleura_form,
        'sesak_napas_form': sesak_napas_form,
        'usg_abdomen_form': usg_abdomen_form,
        'diare_kronis_form': diare_kronis_form,
        'laboratorium_form': laboratorium_form,
        'riwayat_batuk_form': riwayat_batuk_form,
        'riwayat_panas_form': riwayat_panas_form,
        'pembesaran_kgb_form': pembesaran_kgb_form,
        'pemeriksaan_toraks_form': pemeriksaan_toraks_form,
        'luaran_saat_rawat_inap_form': luaran_saat_rawat_inap_form,
        'pemeriksaan_abdomen_form': pemeriksaan_abdomen_form,
        'bta_sputum_aspirat_lambung_form': bta_sputum_aspirat_lambung_form
    }

    return render(request, 'main/form-pasien.html', context=context)


@login_required(login_url='account:login')
def delete_pasien(request, rm_pasien):
    if request.method == 'POST':
        pasien = get_object_or_404(Pasien, rm=rm_pasien)
        pasien.delete()

        return redirect('main:list_pasien')
    return HttpResponseNotAllowed(["POST"])


def get_pasien_data_for_table():
    allValues = []
    for pasien in Pasien.objects.all().order_by('id'):
        pasienList = []
        for name, value in pasien.get_fields():
            if name == "id":
                continue
            if name == "domisili":
                domisili = Domisili.objects.get(id=value)
                for name, valueDomisili in domisili.get_fields():
                    if name == "id":
                        continue
                    pasienList.append(valueDomisili)

            elif name == "tanggal_perawatan":
                tanggal_perawatan = pasien.tanggal_perawatan
                if (not isinstance(tanggal_perawatan, date) and tanggal_perawatan != None):
                    try:
                        tanggal_perawatan = datetime.strptime(
                            tanggal_perawatan, "%d/%m/%Y").date()
                    except ValueError:
                        tanggal_perawatan = None
                pasienList.append(tanggal_perawatan)

            elif name == "jenis_kelamin":
                if pasien.jenis_kelamin == "Laki-laki":
                    pasienList.extend(['1', '-'])
                elif pasien.jenis_kelamin == "Perempuan":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "status_hiv":
                if pasien.status_hiv == "Positif":
                    pasienList.extend(['1', '-', '-'])
                elif pasien.status_hiv == "Negatif":
                    pasienList.extend(['-', '1', '-'])
                elif pasien.status_hiv == "Tidak Diketahui":
                    pasienList.extend(['-', '-', '1'])
                else:
                    pasienList.extend(['-', '-', '-'])

            elif name == "keringat_malam":
                if pasien.keringat_malam == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.keringat_malam == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "edema":
                if pasien.edema == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.edema == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "muntah":
                if pasien.muntah == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.muntah == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "anoreksia":
                if pasien.anoreksia == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.anoreksia == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "hematoskezia":
                if pasien.hematoskezia == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.hematoskezia == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "riwayat_bb_sulit_naik":
                if pasien.riwayat_bb_sulit_naik == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.riwayat_bb_sulit_naik == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "malaise":
                if pasien.malaise == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.malaise == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "riwayat_kontak_tb_dewasa":
                if pasien.riwayat_kontak_tb_dewasa == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.riwayat_kontak_tb_dewasa == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "riwayat_imunisasi_bcg":
                if pasien.riwayat_imunisasi_bcg == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.riwayat_imunisasi_bcg == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "riwayat_pengobatan_tb":
                if pasien.riwayat_pengobatan_tb == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.riwayat_pengobatan_tb == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "efusi_pleura":
                if pasien.efusi_pleura == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.efusi_pleura == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "efusi_perikardium":
                if pasien.efusi_perikardium == "Ya":
                    pasienList.extend(['1', '-'])
                elif pasien.efusi_perikardium == "Tidak":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "tst":
                if pasien.tst == "Dilakukan":
                    pasienList.extend(['1', '-'])
                elif pasien.tst == "Tidak Dilakukan":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "tcm_sputum_aspirat_lambung":
                if pasien.tcm_sputum_aspirat_lambung == "MTB detected":
                    pasienList.extend(['1', '-'])
                elif pasien.tcm_sputum_aspirat_lambung == "MTB Not detected":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(
                        ['Tidak diperiksakan', 'Tidak diperiksakan'])

            elif name == "tcm_asites":
                if pasien.tcm_asites == "MTB detected":
                    pasienList.extend(['1', '-'])
                elif pasien.tcm_asites == "MTB Not detected":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(
                        ['Tidak diperiksakan', 'Tidak diperiksakan'])

            elif name == "tcm_feses":
                if pasien.tcm_feses == "MTB detected":
                    pasienList.extend(['1', '-'])
                elif pasien.tcm_feses == "MTB Not detected":
                    pasienList.extend(['-', '1'])
                else:
                    pasienList.extend(
                        ['Tidak diperiksakan', 'Tidak diperiksakan'])

            elif name == "pendidikan":
                if pasien.pendidikan == "Tidak Bersekolah":
                    pasienList.extend(['1', '-', '-', '-'])
                elif pasien.pendidikan == "SD":
                    pasienList.extend(['-', '1', '-', '-'])
                elif pasien.pendidikan == "SMP":
                    pasienList.extend(['-', '-', '1', '-'])
                elif pasien.pendidikan == "SMA":
                    pasienList.extend(['-', '-', '-', '1'])
                else:
                    pasienList.extend(['-', '-', '-', '-'])

            elif name == "efek_samping_oat":
                if pasien.efek_samping_oat == "ADIH":
                    pasienList.extend(["ADIH"])
                elif pasien.efek_samping_oat == "Tidak ada":
                    pasienList.extend(["Tidak ada"])
                else:
                    pasienList.extend(['-'])

            elif name == "prednison":
                if pasien.prednison == "Ya":
                    pasienList.extend(["1", '-'])
                elif pasien.prednison == "Tidak":
                    pasienList.extend(["-", "1"])
                else:
                    pasienList.extend(['-', '-'])

            elif name == "usia":
                usia = Usia.objects.get(id=value)
                for name, usiaValue in usia.get_fields():
                    if name == "id":
                        continue
                    if name == "kategori":
                        if usiaValue == "< 5 Tahun":
                            pasienList.extend(['1', '-', '-', '-'])
                        elif usiaValue == "5-9 Tahun":
                            pasienList.extend(['-', '1', '-', '-'])
                        elif usiaValue == "10-14 Tahun":
                            pasienList.extend(['-', '-', '1', '-'])
                        elif usiaValue == "15-17 Tahun":
                            pasienList.extend(['-', '-', '-', '1'])
                        else:
                            pasienList.extend(['-', '-', '-', '-'])

                        continue
                    pasienList.append(usiaValue)

            elif name == "status_gizi":
                status_gizi = StatusGizi.objects.get(id=value)
                for name, status_giziValue in status_gizi.get_fields():
                    if name == "id":
                        continue
                    if name == "status_gizi":
                        if status_giziValue == "Normal":
                            pasienList.extend(['1', '-', '-'])
                        elif status_giziValue == "Malnutrisi Sedang":
                            pasienList.extend(['-', '1', '-'])
                        elif status_giziValue == "Malnutrisi Berat":
                            pasienList.extend(['-', '-', '1'])
                        else:
                            pasienList.extend(['-', '-', '-'])
                        continue
                    pasienList.append(status_giziValue)

            elif name == "analisis_cairan_asites":
                analisis_cairan_asites = AnalisisCairanAsites.objects.get(
                    id=value)
                for name, analisis_cairan_asitesValue in analisis_cairan_asites.get_fields():
                    if name == "id":
                        continue
                    pasienList.append(analisis_cairan_asitesValue)

            elif name == "analisis_cairan_pleura":
                analisis_cairan_pleura = AnalisisCairanPleura.objects.get(
                    id=value)
                for name, analisis_cairan_pleuraValue in analisis_cairan_pleura.get_fields():
                    if name == "id":
                        continue
                    pasienList.append(analisis_cairan_pleuraValue)

            elif name == "bta_sputum_aspirat_lambung":
                bta_sputum_aspirat_lambung = BtaSputumAspiratLambung.objects.get(
                    id=value)
                for name, bta_sputum_aspirat_lambungValue in bta_sputum_aspirat_lambung.get_fields():
                    if name == "id":
                        continue
                    else:
                        if bta_sputum_aspirat_lambungValue == "Positif":
                            pasienList.extend(['1', '-'])
                        elif bta_sputum_aspirat_lambungValue == "Negatif":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])

            elif name == "chest_xray":
                chest_xray = ChestXray.objects.get(id=value)
                for name, chest_xrayValue in chest_xray.get_fields():
                    if name != "id":
                        pasienList.append(chest_xrayValue)

            elif name == "diare_kronis":
                diare_kronis = DiareKronis.objects.get(id=value)
                for name, diare_kronisValue in diare_kronis.get_fields():
                    if name == "id":
                        continue
                    if name == "diareKronis":

                        if diare_kronisValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif diare_kronisValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(diare_kronisValue)

            elif name == "laboratorium":
                laboratorium = Laboratorium.objects.get(id=value)
                for name, laboratoriumValue in laboratorium.get_fields():
                    if name == "id":
                        continue
                    pasienList.append(laboratoriumValue)

            elif name == "luaran_saat_rawat_inap":
                luaran_saat_rawat_inap = LuaranSaatRawatInap.objects.get(
                    id=value)
                for name, luaran_saat_rawat_inapValue in luaran_saat_rawat_inap.get_fields():
                    if name != "id":
                        if luaran_saat_rawat_inapValue == "Pulang Paksa":
                            pasienList.extend(['1', '-', '-'])
                        elif luaran_saat_rawat_inapValue == "Perbaikan":
                            pasienList.extend(['-', '1', '-'])
                        elif luaran_saat_rawat_inapValue == "Meninggal":
                            pasienList.extend(['-', '-', '1'])
                        else:
                            pasienList.extend(['-', '-', '-'])

            elif name == "nyeri_perut":
                nyeri_perut = NyeriPerut.objects.get(id=value)
                for name, nyeri_perutValue in nyeri_perut.get_fields():
                    if name == "id":
                        continue
                    if name == "nyeriPerut":
                        if nyeri_perutValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif nyeri_perutValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(nyeri_perutValue)

            elif name == "pembesaran_kgb":
                pembesaran_kgb = PembesaranKgb.objects.get(id=value)
                for name, pembesaran_kgbValue in pembesaran_kgb.get_fields():
                    if name == "id":
                        continue
                    if name == "pembesaran_kgb_utama":
                        if pembesaran_kgbValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif pembesaran_kgbValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(pembesaran_kgbValue)

            elif name == "pemeriksaan_abdomen":
                pemeriksaan_abdomen = PemeriksaanAbdomen.objects.get(id=value)
                for name, pemeriksaan_abdomenValue in pemeriksaan_abdomen.get_fields():
                    if name == "id":
                        continue
                    elif name == "distensi_abdomen":
                        if pemeriksaan_abdomenValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif pemeriksaan_abdomenValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])

                    elif name == "asites":
                        if pemeriksaan_abdomenValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif pemeriksaan_abdomenValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])

                    elif name == "hepatomegali":
                        if pemeriksaan_abdomenValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif pemeriksaan_abdomenValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])

                    elif name == "splenomegali":
                        if pemeriksaan_abdomenValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif pemeriksaan_abdomenValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])

                    elif name == "splenomegali_keterangan":
                        pasienList.append(pemeriksaan_abdomenValue)
                    elif name == "hepatomegali_keterangan":
                        pasienList.append(pemeriksaan_abdomenValue)
                    elif name == "lingkar_perut_masuk":
                        pasienList.append(pemeriksaan_abdomenValue)

            elif name == "pemeriksaan_toraks":
                pemeriksaan_toraks = PemeriksaanToraks.objects.get(id=value)
                for name, pemeriksaan_toraksValue in pemeriksaan_toraks.get_fields():
                    if name == "id":
                        continue
                    if name == "pemeriksaan_toraks":
                        if pemeriksaan_toraksValue == "Normal":
                            pasienList.extend(['1', '-'])
                        elif pemeriksaan_toraksValue == "Abnormal":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(pemeriksaan_toraksValue)

            elif name == "perut_membesar":
                perut_membesar = PerutMembesar.objects.get(id=value)
                for name, perut_membesarValue in perut_membesar.get_fields():
                    if name == "id":
                        continue
                    if name == "perutMembesar":
                        if perut_membesarValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif perut_membesarValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(perut_membesarValue)

            elif name == "riwayat_batuk":
                riwayat_batuk = RiwayatBatuk.objects.get(id=value)
                for name, riwayat_batukValue in riwayat_batuk.get_fields():
                    if name == "id":
                        continue
                    if name == "riwayatBatuk":
                        if riwayat_batukValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif riwayat_batukValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(riwayat_batukValue)

            elif name == "riwayat_panas":
                riwayat_panas = RiwayatPanas.objects.get(id=value)
                for name, riwayat_panasValue in riwayat_panas.get_fields():
                    if name == "id":
                        continue
                    if name == "riwayatPanas":
                        if riwayat_panasValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif riwayat_panasValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(riwayat_panasValue)

            elif name == "sesak_napas":
                sesak_napas = SesakNapas.objects.get(id=value)
                for name, sesak_napasValue in sesak_napas.get_fields():
                    if name == "id":
                        continue
                    if name == "sesakNapas":
                        if sesak_napasValue == "Ya":
                            pasienList.extend(['1', '-'])
                        elif sesak_napasValue == "Tidak":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(['-', '-'])
                        continue
                    pasienList.append(sesak_napasValue)

            elif name == "suhu":
                suhu = Suhu.objects.get(id=value)
                for name, suhuValue in suhu.get_fields():
                    if name == "id":
                        continue
                    pasienList.append(suhuValue)

            elif name == "tcm_pleura":
                tcm_pleura = TcmPleura.objects.get(id=value)
                for name, tcm_pleuraValue in tcm_pleura.get_fields():
                    if name == "id":
                        continue
                    if name == "tcm_pleura":
                        if tcm_pleuraValue == "MTB detected":
                            pasienList.extend(['1', '-'])
                        elif tcm_pleuraValue == "MTB Not detected":
                            pasienList.extend(['-', '1'])
                        else:
                            pasienList.extend(
                                ['Tidak Diperiksakan', 'Tidak Diperiksakan'])
                        continue
                    else:
                        pasienList.append(tcm_pleuraValue)

            elif name == "usg_abdomen":
                usg_abdomen = UsgAbdomen.objects.get(id=value)
                for name, usg_abdomenValue in usg_abdomen.get_fields():
                    if name != "id":
                        pasienList.append(usg_abdomenValue)

            else:
                pasienList.append(value)

        allValues.append(pasienList)
    return allValues


@login_required(login_url='account:login')
def list_pasien(request):
    if request.method == "GET":
        context = {
            "excel_upload_form": ExcelUploadForm(),
            "headers": getHeader(),
            "allValues": get_pasien_data_for_table()
        }
        return render(request, 'main/list-pasien.html', context)

    if request.method == "POST":
        context = {"excel_upload_form": ExcelUploadForm()}
        return post_import_excel_page(request, context)

    return HttpResponseNotAllowed(["GET", "POST"])

def delete_all_objects():
    Pasien.objects.all().delete()
    AnalisisCairanAsites.objects.all().delete()
    AnalisisCairanPleura.objects.all().delete()
    BtaSputumAspiratLambung.objects.all().delete()
    ChestXray.objects.all().delete()
    DiareKronis.objects.all().delete()
    Domisili.objects.all().delete()
    Laboratorium.objects.all().delete()
    LuaranSaatRawatInap.objects.all().delete()
    NyeriPerut.objects.all().delete()
    PembesaranKgb.objects.all().delete()
    PemeriksaanAbdomen.objects.all().delete()
    PemeriksaanToraks.objects.all().delete()
    PerutMembesar.objects.all().delete()
    RiwayatBatuk.objects.all().delete()
    RiwayatPanas.objects.all().delete()
    SesakNapas.objects.all().delete()
    StatusGizi.objects.all().delete()
    Suhu.objects.all().delete()
    TcmPleura.objects.all().delete()
    UsgAbdomen.objects.all().delete()
    Usia.objects.all().delete()

def ya_tidak(row, satu, dua):
    if row[satu] == '-':
        if row[dua] == '-':
            return None
        else:
            return 'Tidak'
    else:
        return 'Ya'

def positif_negatif(row, satu, dua):
    if row[satu] == '-':
        if row[dua] == '-':
            return None
        else:
            return 'Negatif'
    else:
        return 'Positif'

@login_required(login_url='account:login')
def post_import_excel_page(request, response):
    
    form = ExcelUploadForm(request.POST, request.FILES)
    if form.is_valid():
        workbook = load_workbook(request.FILES["excel_file"])
        sheet = workbook.active

        delete_all_objects()
        
        for row in sheet.iter_rows(min_row=4, values_only=True):
            if(row[0] == None or str(row[0]).lower() == 'jumlah' or len(row) < 293):
                break

            analisis_cairan_asites = AnalisisCairanAsites(warna_asites=row[151], rivalta_asites=row[152], jumlah_sel_asites=row[153], pmn_asites=row[
                                                          154], mn_asites=row[155], glukosa_asites=row[156], albumin_asites=row[157], protein_asites=row[158], ldh_asites=row[159])
            analisis_cairan_asites.save()

            analisis_cairan_pleura = AnalisisCairanPleura(warna_pleura=row[160], rivalta_pleura=row[161], jumlah_sel_pleura=row[162], pmn_pleura=row[
                                                          163], mn_pleura=row[164], glukosa_pleura=row[165], protein_pleura=row[166], albumin_pleura=row[167], ldh_pleura=row[168])
            analisis_cairan_pleura.save()


            bta_1 = positif_negatif(row, 132, 133)
            bta_2 = positif_negatif(row, 134, 135)
            bta_3 = positif_negatif(row, 136, 137)
            bta_sputum_aspirat_lambung = BtaSputumAspiratLambung(
                bta_1=bta_1, bta_2=bta_2, bta_3=bta_3)
            bta_sputum_aspirat_lambung.save()

            chest_xray = ChestXray(normal=row[122], klasifikasi_dengan_infiltat=row[123], pembesaran_kgb_hilus_atau_paratrakeal=row[124], kavitas=row[125],
                                   efusi_pleura_chest_xray=row[126], konsolidasi=row[127], milier=row[128], tuberkoloma=row[129], atelektasis=row[130], ekspertise=row[131])
            chest_xray.save()

            field_diare_kronis = ya_tidak(row,49,50)
            diare_kronis = DiareKronis(
                diareKronis=field_diare_kronis, keterangan_diare_kronis=row[51])
            diare_kronis.save()

            domisili = Domisili(bandung=row[8], luarBandung=row[9])
            domisili.save()

            laboratorium = Laboratorium(hb=row[98], ht=row[99], leukosit=row[100], trombosit=row[101], basofil=row[102], eosinofil=row[103], n_batang=row[104], n_segemen=row[105],
                                        limfosit=row[106], monosit=row[107], sgot=row[108], sgpt=row[109], bil_total=row[110], bil_direk=row[111], albumin=row[112], crp=row[113])
            laboratorium.save()

            status = 'Pulang Paksa'
            if row[177] == '-':
                if row[178] == '-':
                    if row[179] == '-':
                        status = None
                    else:
                        status = 'Meninggal'
                else:
                    status = 'Perbaikan'

            luaran_saat_rawat_inap = LuaranSaatRawatInap(status=status)
            luaran_saat_rawat_inap.save()

            field_nyeri_perut = ya_tidak(row, 35, 36)
            
            nyeri_perut = NyeriPerut(
                nyeriPerut=field_nyeri_perut, keterangan_nyeri_perut=row[37])
            nyeri_perut.save()

            pembesaran_kgb_utama = ya_tidak(row, 77, 78)
            
            pembesaran_kgb = PembesaranKgb(
                pembesaran_kgb_utama=pembesaran_kgb_utama, lokasi=row[79])
            pembesaran_kgb.save()

            field_distensi_abdomen = ya_tidak(row,83,84)
            field_asites = ya_tidak(row,85,86)
            field_hepatomegali = ya_tidak(row,87,88)
            field_splenomegali = ya_tidak(row,90,91)
            pemeriksaan_abdomen = PemeriksaanAbdomen(distensi_abdomen=field_distensi_abdomen, asites=field_asites, hepatomegali=field_hepatomegali,
                                                     hepatomegali_keterangan=row[89], splenomegali=field_splenomegali, splenomegali_keterangan=row[92], lingkar_perut_masuk=row[93])
            pemeriksaan_abdomen.save()

            if str(row[80]) == '1':
                field_pemeriksaan_toraks = 'Normal'
            elif str(row[81]) == '1':
                field_pemeriksaan_toraks = 'Abnormal'
            else:
                field_pemeriksaan_toraks = None

            pemeriksaan_toraks = PemeriksaanToraks(
                pemeriksaan_toraks=field_pemeriksaan_toraks, deskripsi=row[82])
            pemeriksaan_toraks.save()
            
            field_perut_membesar = ya_tidak(row,32,33)

            perut_membesar = PerutMembesar(
                perutMembesar=field_perut_membesar, keterangan_perut_membesar=row[34])
            perut_membesar.save()

            field_riwayat_batuk = ya_tidak(row, 57, 58)
            riwayat_batuk = RiwayatBatuk(
                riwayatBatuk=field_riwayat_batuk, keterangan_riwayat_batuk=row[59])
            riwayat_batuk.save()

            field_riwayat_panas = ya_tidak(row, 54, 55)
            riwayat_panas = RiwayatPanas(
                riwayatPanas=field_riwayat_panas, keterangan_riwayat_panas=row[56])
            riwayat_panas.save()

            field_sesak_napas = ya_tidak(row, 40, 41)
            sesak_napas = SesakNapas(
                sesakNapas=field_sesak_napas, keterangan_sesak_napas=row[42])
            sesak_napas.save()

            field_status_gizi = 'Normal'
            if row[19] == '-':
                if row[20] == '-':
                    field_status_gizi = 'Malnutrisi Berat'
                    if row[21] == '-':
                        field_status_gizi = None
                else:
                    field_status_gizi = 'Malnutrisi Sedang'
            status_gizi = StatusGizi(
                status_gizi=field_status_gizi, BB=row[22], TB=row[23], LLA=row[24], TBU=row[25], BMIU=row[26], BBTB=row[27], LLAU=row[28])
            status_gizi.save()

            suhu = Suhu(febris=row[75], suhu=row[76])
            suhu.save()

            field_tcm_pleura = 'Tidak diperiksakan'
            if row[142] == '-':
                field_tcm_pleura = 'MTB Not detected'
            elif row[143] == '-':
                field_tcm_pleura = 'MTB detected'
            tcm_pleura = TcmPleura(
                tcm_pleura=field_tcm_pleura, keterangan_tcm_pleura=row[144])
            tcm_pleura.save()

            usg_abdomen = UsgAbdomen(pembesaran_kgb=row[117], asites_usg_abdomen=row[118],
                                     hepatosplenomegali=row[119], lain_lain=row[120], ekspertise=row[121])
            usg_abdomen.save()

            kategori_usia = '< 5 Tahun'
            if row[15] == '-':
                if row[16] == '-':
                    if row[17] == '-':
                        kategori_usia = '15-17 Tahun'
                        if row[18] == '-':
                            kategori_usia = None
                    else:
                        kategori_usia = '10-14 Tahun'
                else:
                    kategori_usia = '5-9 Tahun'
            usia = Usia(usia=row[14], kategori=kategori_usia)
            usia.save()

            jenis_kelamin = ""
            # print("row5{}".format(row[5]))
            # print("row4{}".format(row[6]))
            if str(row[5]) == "-" and str(row[6]) == "1":
                jenis_kelamin = "Perempuan"
            elif str(row[5]) == "1" and str(row[6]) == "-":
                jenis_kelamin = "Laki-laki"
            else:
                jenis_kelamin = None
                
            pendidikan = 'Tidak Bersekolah'
            if row[10] == '-':
                if row[11] == '-':
                    if row[12] == '-':
                        pendidikan = 'SMA'
                        if row[13] == '-':
                            pendidikan = None
                    else:
                        pendidikan = 'SMP'
                else:
                    pendidikan = 'SD'

            status_hiv = 'Positif'
            if row[29] == '-':
                if row[30] == '-':
                    status_hiv = 'Tidak diketahui'
                    if row[31] == '-':
                        status_hiv = None
                else:
                    status_hiv = 'Negatif'

            keringat_malam = ya_tidak(row, 38, 39)
            edema = ya_tidak(row,43,44)
            muntah = ya_tidak(row,45,46)
            anoreksia = ya_tidak(row,47,48)
            hematoskezia = ya_tidak(row,52,53)
            riwayat_bb_sulit_naik = ya_tidak(row,60,61)
            malaise = ya_tidak(row,63,64)
            riwayat_kontak_tb_dewasa = ya_tidak(row, 65, 66)
            riwayat_imunisasi_bcg = ya_tidak(row, 68, 69)
            riwayat_pengobatan_tb = ya_tidak(row, 70, 71)
            efusi_pleura = ya_tidak(row, 94, 65)
            efusi_perikardium = ya_tidak(row, 96, 97)
            
            if str(row[114]) == '1':
                tst = 'Dilakukan'
            elif str(row[115]) == '1':
                tst = 'Tidak Dilakukan'
            else:
                tst = None
                


            tcm_sputum_aspirat_lambung = 'Tidak diperiksakan'
            if row[138] == '-':
                tcm_sputum_aspirat_lambung = 'MTB Not detected'
            elif row[139] == '-':
                tcm_sputum_aspirat_lambung = 'MTB detected'

            tcm_asites = 'Tidak diperiksakan'
            if row[140] == '-':
                tcm_asites = 'MTB Not detected'
            elif row[141] == '-':
                tcm_asites = 'MTB detected'            

            tcm_feses = 'Tidak diperiksakan'
            if row[145] == '-':
                tcm_asites = 'MTB Not detected'
            elif row[146] == '-':
                tcm_asites = 'MTB detected'
  
            prednison = ya_tidak(row, 172, 173)
            tanggal_perawatan = row[2]
            if (not isinstance(tanggal_perawatan, date)):
                if (tanggal_perawatan != None):
                    try:
                        tanggal_perawatan = datetime.strptime(
                            tanggal_perawatan, "%d/%m/%Y").date()
                    except ValueError:
                        tanggal_perawatan = None

            tanggal_mulai_oat=row[182]
            if (not isinstance(tanggal_mulai_oat, date)):
                if (tanggal_mulai_oat != None):
                    try:
                        tanggal_mulai_oat = datetime.strptime(
                            tanggal_mulai_oat, "%d/%m/%Y").date()
                    except ValueError:
                        tanggal_mulai_oat = None

            pasien = Pasien(rm=row[1], tanggal_perawatan=tanggal_perawatan, no_hp=row[3], nama_pasien=row[4], jenis_kelamin=jenis_kelamin, rujukan_dari=row[7], domisili=domisili, pendidikan=pendidikan, usia=usia, status_gizi=status_gizi, status_hiv=status_hiv, perut_membesar=perut_membesar, nyeri_perut=nyeri_perut, keringat_malam=keringat_malam, sesak_napas=sesak_napas, edema=edema, muntah=muntah, anoreksia=anoreksia, diare_kronis=diare_kronis, hematoskezia=hematoskezia, riwayat_panas=riwayat_panas, riwayat_batuk=riwayat_batuk, riwayat_bb_sulit_naik=riwayat_bb_sulit_naik, penurunan_bb=row[62], malaise=malaise, riwayat_kontak_tb_dewasa=riwayat_kontak_tb_dewasa, siapa=row[67], riwayat_imunisasi_bcg=riwayat_imunisasi_bcg, riwayat_pengobatan_tb=riwayat_pengobatan_tb, kapan_pengobatan_tb_sebelumnya=row[72], durasi_pengobatan_tb_sebelumnya=row[73], keadaan_umum=row[74], suhu=suhu,
                            pembesaran_kgb=pembesaran_kgb, pemeriksaan_toraks=pemeriksaan_toraks, pemeriksaan_abdomen=pemeriksaan_abdomen, efusi_pleura=efusi_pleura, efusi_perikardium=efusi_perikardium, laboratorium=laboratorium, tst=tst, tst_mm=row[116], usg_abdomen=usg_abdomen, chest_xray=chest_xray, bta_sputum_aspirat_lambung=bta_sputum_aspirat_lambung, tcm_sputum_aspirat_lambung=tcm_sputum_aspirat_lambung, tcm_asites=tcm_asites, tcm_pleura=tcm_pleura, tcm_feses=tcm_feses, kultur_mtb=row[147], lcs=row[148], fnab_pa_jaringan=row[149], ct_scan_abdomen=row[150], analisis_cairan_asites=analisis_cairan_asites, analisis_cairan_pleura=analisis_cairan_pleura, diagnosis_akhir=row[169], diagnosis_komorbiditas=row[170], regimen_oat=row[171], prednison=prednison, efek_samping_oat=row[174], regimen_oat_diganti_dengan=row[175], los=row[176], luaran_saat_rawat_inap=luaran_saat_rawat_inap,
                            keterangan_lain=row[180], dpjp_utama=row[181], tanggal_mulai_oat=tanggal_mulai_oat, status_gizi_1=row[183], keluhan_1=row[184], pemeriksaan_fisik_1=row[185], lab_1=row[186], radiologi_1=row[187], regimen_oat_1=row[188], diagnosis_1=row[189], komplikasi_1=row[190], lain_lain_1=row[191], status_gizi_2=row[192], keluhan_2=row[193], pemeriksaan_fisik_2=row[194], lab_2=row[195], radiologi_2=row[196], regimen_oat_2=row[197], diagnosis_2=row[198], komplikasi_2=row[199], lain_lain_2=row[200], status_gizi_3=row[201], keluhan_3=row[202], pemeriksaan_fisik_3=row[203], lab_3=row[204], radiologi_3=row[205], regimen_oat_3=row[206], diagnosis_3=row[207], komplikasi_3=row[208], lain_lain_3=row[209], status_gizi_4=row[210], keluhan_4=row[211], pemeriksaan_fisik_4=row[212], lab_4=row[213], radiologi_4=row[214], regimen_oat_4=row[215], diagnosis_4=row[216], komplikasi_4=row[217], 
                            lain_lain_4=row[218], status_gizi_5=row[219], keluhan_5=row[220], pemeriksaan_fisik_5=row[221], lab_5=row[222], radiologi_5=row[223], regimen_oat_5=row[224], diagnosis_5=row[225], komplikasi_5=row[226], lain_lain_5=row[227], status_gizi_6=row[228], keluhan_6=row[229], pemeriksaan_fisik_6=row[230], lab_6=row[231], radiologi_6=row[232], regimen_oat_6=row[233], diagnosis_6=row[234], komplikasi_6=row[235], lain_lain_6=row[236], status_gizi_7=row[237], keluhan_7=row[238], pemeriksaan_fisik_7=row[239], lab_7=row[240], radiologi_7=row[241], regimen_oat_7=row[242], diagnosis_7=row[243], komplikasi_7=row[244], lain_lain_7=row[245], status_gizi_8=row[246], keluhan_8=row[247], pemeriksaan_fisik_8=row[248], lab_8=row[249], radiologi_8=row[250], regimen_oat_8=row[251], diagnosis_8=row[252], komplikasi_8=row[253], lain_lain_8=row[254], status_gizi_9=row[255], 
                            keluhan_9=row[256], pemeriksaan_fisik_9=row[257], lab_9=row[258], radiologi_9=row[259], regimen_oat_9=row[260], diagnosis_9=row[261], komplikasi_9=row[262], lain_lain_9=row[263], status_gizi_10=row[264], keluhan_10=row[265], pemeriksaan_fisik_10=row[266], lab_10=row[267], radiologi_10=row[268], regimen_oat_10=row[269], diagnosis_10=row[270], komplikasi_10=row[271], lain_lain_10=row[272], status_gizi_11=row[273], keluhan_11=row[274], pemeriksaan_fisik_11=row[275], lab_11=row[276], radiologi_11=row[277], regimen_oat_11=row[278], diagnosis_11=row[279], komplikasi_11=row[280], lain_lain_11=row[281], status_gizi_12=row[282], keluhan_12=row[283], pemeriksaan_fisik_12=row[284], lab_12=row[285], radiologi_12=row[286], regimen_oat_12=row[287], diagnosis_12=row[288], komplikasi_12=row[289], lain_lain_12=row[290], durasi_oat=row[291], hasil_akhir_pengobatan=row[292])
            pasien.save()

    return redirect(reverse_lazy('main:list_pasien'))


def export_to_excel():
    wb = Workbook()
    wb.iso_dates = True

    ws1 = wb.active
    ws1.title = 'Pasien'

    list_title = ['No', 'RM', 'Tgl. Perawatan', 'No. HP', 'Nama Pasien']

    make_blank_cell(list_title, 2, 'Jenis Kelamin')

    list_title.append('Rujukan dari')

    make_blank_cell(list_title, 2, 'Domisili')

    make_blank_cell(list_title, 4, 'Pendidikan')

    make_blank_cell(list_title, 5, 'Usia (tahun)')

    make_blank_cell(list_title, 10, 'Status Gizi')

    make_blank_cell(list_title, 3, 'Status HIV')

    make_blank_cell(list_title, 3, 'Perut Membesar')

    make_blank_cell(list_title, 3, 'Nyeri Perut')

    make_blank_cell(list_title, 2, 'Keringat Malam')

    make_blank_cell(list_title, 3, 'Sesak Napas')

    make_blank_cell(list_title, 2, 'Edema')

    make_blank_cell(list_title, 2, 'Muntah')

    make_blank_cell(list_title, 2, 'Anoreksia')

    make_blank_cell(list_title, 3, 'Diare Kronis')

    make_blank_cell(list_title, 2, 'Hematoskezia/bleed per rektum')

    make_blank_cell(list_title, 3, 'Riwayat Panas ≥ 2 Minggu')

    make_blank_cell(list_title, 3, 'Riwayat Batuk ≥ 2 Minggu')

    make_blank_cell(list_title, 2, 'Riwayat BB Sulit Naik')

    list_title.append('Penurunan BB')

    make_blank_cell(list_title, 2, 'Malaise')

    make_blank_cell(list_title, 2, 'Riwayat Kontak TB Dewasa')

    list_title.append('Siapa')

    make_blank_cell(list_title, 2, 'Riwayat Imunisasi BCG')

    make_blank_cell(list_title, 2, 'Riwayat Pengobatan TB')

    list_title.append('Kapan Pengobatan TB Sebelumbya')

    list_title.append('Durasi Pengobatan TB Sebelumnya')

    list_title.append('Keadaan Umum')

    make_blank_cell(list_title, 2, 'Suhu')

    make_blank_cell(list_title, 3, 'Pembesaran kgb')

    make_blank_cell(list_title, 3, 'Pemeriksaan Toraks')

    make_blank_cell(list_title, 11, 'Pemeriksaan Abdomen')

    make_blank_cell(list_title, 2, 'Efisi Pleura')

    make_blank_cell(list_title, 2, 'Efisi Perikardium')

    make_blank_cell(list_title, 16, 'Laboratorium')

    make_blank_cell(list_title, 2, 'TST')

    list_title.append('TST (mm)')

    make_blank_cell(list_title, 5, 'USG Abdomen')

    make_blank_cell(list_title, 10, 'Chest X-Ray')

    make_blank_cell(list_title, 6, 'BTA Sputum/Aspirat Lambung')

    make_blank_cell(list_title, 2, 'TCM Sputum/ Aspirat Lambung')

    make_blank_cell(list_title, 2, 'TCM Asites')

    make_blank_cell(list_title, 3, 'TCM Pleura')

    make_blank_cell(list_title, 2, 'TCM Feses')

    list_title.append('Kultur M.Tb')

    list_title.append('LCS')

    list_title.append('FNAB/PA Jaringan')

    list_title.append('CT Scan Abdomen')

    make_blank_cell(list_title, 9, 'Analisis Cairan Asites')

    make_blank_cell(list_title, 9, 'Analisis Cairan Pleura')

    list_title.append('Diagnosis Akhir')

    list_title.append('Diagnosis Kormobiditas')

    list_title.append('Regimen OAT')

    make_blank_cell(list_title, 2, 'Prednison')

    list_title.append('Efek Samping OAT yang Muncul')

    list_title.append('Regimen OAT diganti dengan')

    list_title.append('LOS (Hari)')

    make_blank_cell(list_title, 3, 'Luaran Saat Rawat Inap')

    list_title.extend(['Keterangan Lain','DPJP Utama', 'Tanggal Mulai OAT'])

    make_blank_cell(list_title, 9, 'Bulan Ke-1')

    make_blank_cell(list_title, 9, 'Bulan Ke-2')

    make_blank_cell(list_title, 9, 'Bulan Ke-3')

    make_blank_cell(list_title, 9, 'Bulan Ke-4')

    make_blank_cell(list_title, 9, 'Bulan Ke-5')

    make_blank_cell(list_title, 9, 'Bulan Ke-6')

    make_blank_cell(list_title, 9, 'Bulan Ke-7')

    make_blank_cell(list_title, 9, 'Bulan Ke-8')

    make_blank_cell(list_title, 9, 'Bulan Ke-9')

    make_blank_cell(list_title, 9, 'Bulan Ke-10')

    make_blank_cell(list_title, 9, 'Bulan Ke-11')

    make_blank_cell(list_title, 9, 'Bulan Ke-12')

    list_title.extend(['Durasi OAT', 'Hasil Akhir Pengobatan'])

    ws1.append(list_title)
    list_title.clear()


    make_blank_cell(list_title, 83, 'blank')

    make_blank_cell(list_title, 2, 'Distensi Abdomen')

    make_blank_cell(list_title, 2, 'Asites')

    make_blank_cell(list_title, 3, 'Hepatomegali')

    make_blank_cell(list_title, 3, 'Splenomegali')

    list_title.append('Lingkar Perut Masuk')

    make_blank_cell(list_title, 38, 'blank')

    make_blank_cell(list_title, 2, '1')

    make_blank_cell(list_title, 2, '2')

    make_blank_cell(list_title, 2, '3')

    make_blank_cell(list_title, 39, 'blank')

    make_blank_cell(list_title, 2, 'Hidup')

    list_title.append('Meninggal')

    make_blank_cell(list_title, 77, 'blank')


    ws1.append(list_title)
    list_title.clear()


    make_blank_cell(list_title, 5, 'blank')

    list_title += ['L', 'P', 'blank', 'Bandung', 'Luar Bandung', 'Tidak Bersekolah', 'SD', 'SMP', 'SMA', 'Usia', '<5 Thn', '5-9 Thn', '10-14 Thn', '15-17 Thn',
                   'Normal', 'Malnutrisi Sedang', 'Malnutrisi Berat', 'BB (kg)', 'TB (cm)', 'LLA', 'TB/U', 'BMI/U', 'BB/TB', 'LLA/U', 'Positif', 'Negatif', 'Tidak Diketahui']

    yt = ['Ya', 'Tidak']
    ytk = ['Ya', 'Tidak', 'Ket']

    list_title += ytk   # Perut Membesar
    list_title += ytk   # Nyeri Perut
    list_title += yt    # Keringat Malam
    list_title += ytk   # Sesak Napas
    list_title += yt    # Edema
    list_title += yt    # Muntah
    list_title += yt    # Anoreksia
    list_title += ytk   # Diare Kronis
    list_title += yt    # Hematoskezia
    list_title += ytk   # Riwayat Panas
    list_title += ytk   # Riwaya Batuk
    list_title += yt    # Riwayat BB
    list_title.append('blank')
    list_title += yt   # Malaise
    list_title += yt   # Riwayat Kontak TB
    list_title.append('blank')
    list_title += yt    # Riwayat Imunisasi
    list_title += yt    # Riwayat Pengobatan
    make_blank_cell(list_title, 3, 'blank')
    list_title += ['Febris', 'Suhu']
    list_title += yt    # Pembesaran KGB
    list_title += ['Lokasi', 'Normal', 'Abnormal', 'Deskripsi']
    list_title += yt
    list_title += yt
    list_title += ytk
    list_title += ytk
    list_title.append('blank')
    list_title += yt
    list_title += yt
    list_title += ['Hb', 'Ht', 'Leukosit', 'Trombosit', 'Basofil', 'Eosinofil', 'N. Barang', 'N. Segemen', 'Limfosit', 'Monosit', 'SGOT', 'SGPT', 'Bil. Total', 'Bil. Direk', 'Albumin', 'CRP', 'Dilakukan', 'Tidak Dilakukan', 'blank', 'Pembesaran KGB', 'Asites', 'Hepatosplenomegali', 'Lain-lain', 'Ekspertise', 'Normal', 'Klasifijasu dengan Infiltat',
                   'Pembesaran KGB Hilus atau Paratrakeal', 'Kavitas', 'Evusi Pleura', 'Konsolidasi', 'Milier', 'Tuberkoloma', 'Atelektasis', 'Ekspertise', 'Positif', 'Negatif', 'Positif', 'Negatif', 'Positif', 'Negatif', 'MTB Detected', 'MTB Not Detected', 'MTB Detected', 'MTB Not Detected', 'MTB Detected', 'MTB Not Detected', 'Keterangan', 'MTB Detected', 'MTB Not Detected']
    make_blank_cell(list_title, 4, 'blank')
    list_title += ['Warna', 'Rivalta', 'Jumlah Sel', 'PMN',
                   'MN', 'Glukosa', 'Albumin', 'Protein', 'LDH']
    list_title += ['Warna', 'Rivalta', 'Jumlah Sel', 'PMN',
                   'MN', 'Glukosa', 'Albumin', 'Protein', 'LDH']
    make_blank_cell(list_title, 3, 'blank')
    list_title += yt
    make_blank_cell(list_title, 4, 'blank')
    list_title += ['Pulang Paksa', 'Perbaikan']

    make_blank_cell(list_title, 3, 'blank')

    list_title += ['Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain', 
    'blank', 'blank']

    ws1.append(list_title)
    list_title.clear()

    # Masukin Data
    all_data = get_pasien_data_for_table()
    num_of_data = len(all_data)

    for i in range(num_of_data):
        ws1.append(all_data[i])

    ws1.move_range(f'A4:KF{num_of_data + 3}', cols=1)

    for i in range(1, num_of_data + 1):
        ws1[f'A{i + 3}'] = i

    # Pengaturan ukuran Column
    ws1.column_dimensions[get_column_letter(1)].width = 7

    for col in range(2, 6):
        ws1.column_dimensions[get_column_letter(col)].width = 22

    for col in range(6, 294):
        ws1.column_dimensions[get_column_letter(col)].width = 15

        pendek = ['L', 'P', 'Tidak Bersekolah', 'SD', 'SMP', 'SMA', '<5 Thn', '5-9 Thn', '10-14 Thn',
                  '15-17 Thn', 'Positif', 'Negatif', 'Tidak Diketahui', 'Ya', 'Tidak', 'Normal', 'Abnormal']
        sedang = [175, 176, 177]
        sedang2 = ['Status Gizi', 'Keluhan', 'Pemeriksaan Fisik', 'Lab', 'Radiologi', 'Regimen OAT', 'Diagnosis', 'Komplikasi', 'Lain-lain']
        panjang = [148, 149, 150, 151, 170, 171]

        if (ws1.cell(3, col).value in pendek):
            ws1.column_dimensions[get_column_letter(col)].width = 10

        elif (col in sedang):
            ws1.column_dimensions[get_column_letter(col)].width = 25

        elif (col in panjang):
            ws1.column_dimensions[get_column_letter(col)].width = 50

        elif (ws1.cell(3, col).value in sedang2):
            ws1.column_dimensions[get_column_letter(col)].width = 27

    # Pengaturan ukuran Row

    ws1.row_dimensions[1].height = 45
    ws1.row_dimensions[2].height = 45
    ws1.row_dimensions[3].height = 40

    for row in range(4, num_of_data + 4):
        ws1.row_dimensions[row].height = 70

    # Styling

    for row in range(1, num_of_data + 4):
        for col in range(1, 294):
            ws1["{}{}".format(get_column_letter(col), row)].alignment = Alignment(
                horizontal="center", vertical="center")

            ws1[f'{get_column_letter(col)}{row}'].border = Border(left=Side(border_style="thin",
                                                                            color='00000000'),
                                                                  right=Side(border_style="thin",
                                                                             color='00000000'),
                                                                  top=Side(border_style="thin",
                                                                           color='00000000'),
                                                                  bottom=Side(border_style="thin",
                                                                              color='00000000'),
                                                                  )

            if (ws1.cell(row, 293).value != None):
                if ('masih dalam pengobatan' in ws1.cell(row, 293).value.lower()):
                    ws1[f'{get_column_letter(col)}{row}'].fill = PatternFill("solid", fgColor="008cdc")
                elif ('meninggal' in ws1.cell(row, 293).value.lower()):
                    ws1[f'{get_column_letter(col)}{row}'].fill = PatternFill("solid", fgColor="ff0707")

            kuning = [1, 2, 3]
            if(row in kuning and col < 183):
                ws1[f'{get_column_letter(col)}{row}'].fill = PatternFill( "solid", fgColor="ffff00")

            elif (row in kuning and col >= 183):
                ws1[f'{get_column_letter(col)}{row}'].fill = PatternFill("solid", fgColor="79C94C")

    for row in ws1.iter_rows():
        for cell in row:
            alignment = copy.copy(cell.alignment)
            alignment.wrapText = True
            cell.alignment = alignment

    # Merging Cell
    def merge_row(row, start, end):
        ws1.merge_cells(start_row=row, end_row=row,
                        start_column=start, end_column=end)

    def merge_2_row(start, end):
        ws1.merge_cells(start_row=1, end_row=2,
                        start_column=start, end_column=end)

    def merge_3_row(col):
        ws1.merge_cells(start_row=1, end_row=3,
                        start_column=col, end_column=col)

    # A-E
    for i in range(5):
        merge_3_row(i+1)
    # F-G
    merge_2_row(6, 7)

    merge_3_row(8)

    merge_2_row(9, 10)

    merge_2_row(11, 14)

    merge_2_row(15, 19)

    merge_2_row(20, 29)  # Status Gizi

    merge_2_row(30, 32)  # Status HIV

    merge_2_row(33, 35)

    merge_2_row(36, 38)

    merge_2_row(39, 40)

    merge_2_row(41, 43)

    merge_2_row(44, 45)

    merge_2_row(46, 47)

    merge_2_row(48, 49)

    merge_2_row(50, 52)

    merge_2_row(53, 54)

    merge_2_row(55, 57)

    merge_2_row(58, 60)

    merge_2_row(61, 62)

    merge_3_row(63)

    merge_2_row(64, 65)

    merge_2_row(66, 67)

    merge_3_row(68)

    merge_2_row(69, 70)

    merge_2_row(71, 72)

    merge_3_row(73)

    merge_3_row(74)

    merge_3_row(75)

    merge_2_row(76, 77)

    merge_2_row(78, 80)

    merge_2_row(81, 83)

    merge_row(1, 84, 94)

    merge_row(2, 84, 85)

    merge_row(2, 86, 87)

    merge_row(2, 88, 90)

    merge_row(2, 91, 93)

    ws1.merge_cells(start_row=2, end_row=3, start_column=94, end_column=94)

    merge_2_row(95, 96)

    merge_2_row(97, 98)

    merge_2_row(99, 114)

    merge_2_row(115, 116)

    merge_3_row(117)

    merge_2_row(118, 122)

    merge_2_row(123, 132)

    merge_row(1, 133, 138)

    merge_row(2, 133, 134)

    merge_row(2, 135, 136)

    merge_row(2, 137, 138)

    merge_2_row(139, 140)

    merge_2_row(141, 142)

    merge_2_row(143, 145)

    merge_2_row(146, 147)

    for i in range(148, 152):
        merge_3_row(i)

    merge_2_row(152, 160)

    merge_2_row(161, 169)

    for i in range(170, 173):
        merge_3_row(i)

    merge_2_row(173, 174)

    for i in range(175, 178):
        merge_3_row(i)

    merge_row(1, 178, 180)

    merge_row(2, 178, 179)

    ws1.merge_cells(start_row=2, end_row=3, start_column=180, end_column=180)

    merge_3_row(181)

    merge_3_row(182)

    merge_3_row(183)

    merge_2_row(184, 192)

    merge_2_row(193, 201)

    merge_2_row(202, 210)

    merge_2_row(211, 219)

    merge_2_row(220, 228)

    merge_2_row(229, 237)

    merge_2_row(238, 246)

    merge_2_row(247, 255) 

    merge_2_row(256, 264)

    merge_2_row(265, 273) # 10

    merge_2_row(274, 282)

    merge_2_row(283, 291)

    merge_3_row(292)

    merge_3_row(293)

    ws1.freeze_panes = "F4"

    return wb


@login_required(login_url='account:login')
def download(_):
    response = HttpResponse(save_virtual_workbook(
        export_to_excel()), content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = f'attachment; filename="export_pasien_{datetime.now()}.xlsx"'
    return response


def make_blank_cell(lst, n, st):
    for i in range(n):
        if (i == 0):
            lst.append(st)
        else:
            lst.append('blank')
            

