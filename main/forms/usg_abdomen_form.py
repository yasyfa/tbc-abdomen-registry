from django import forms

from ..models.usg_abdomen import UsgAbdomen

class UsgAbdomenForm(forms.ModelForm):
    class Meta:
        model = UsgAbdomen
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UsgAbdomenForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'