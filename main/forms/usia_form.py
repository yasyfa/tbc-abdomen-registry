from django import forms

from ..models.usia import Usia

class UsiaForm(forms.ModelForm):
    class Meta:
        model = Usia
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UsiaForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'