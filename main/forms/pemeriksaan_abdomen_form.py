from django import forms

from ..models.pemeriksaan_abdomen import PemeriksaanAbdomen

class PemeriksaanAbdomenForm(forms.ModelForm):
    class Meta:
        model = PemeriksaanAbdomen
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PemeriksaanAbdomenForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'