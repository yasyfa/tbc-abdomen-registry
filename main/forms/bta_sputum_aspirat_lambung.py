from ..models.bta_sputum_aspirat_lambung import BtaSputumAspiratLambung

from django import forms

class BtaSputumAspiratLambungForm(forms.ModelForm):
    class Meta:
        model = BtaSputumAspiratLambung
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(BtaSputumAspiratLambungForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'