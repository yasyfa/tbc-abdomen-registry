from django import forms

from ..models.laboratorium import Laboratorium

class LaboratoriumForm(forms.ModelForm):
    class Meta:
        model = Laboratorium
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(LaboratoriumForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'