from django import forms

from ..models.suhu import Suhu


class SuhuForm(forms.ModelForm):
    class Meta:
        model = Suhu
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SuhuForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'