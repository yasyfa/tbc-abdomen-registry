from django import forms

from ..models.domisili import Domisili

class DomisiliForm(forms.ModelForm):
    class Meta:
        model = Domisili
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DomisiliForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'