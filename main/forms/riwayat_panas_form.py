from django import forms

from ..models.riwayat_panas import RiwayatPanas

class RiwayatPanasForm(forms.ModelForm):
    class Meta:
        model = RiwayatPanas
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RiwayatPanasForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'