from django import forms

from ..models.nyeri_perut import NyeriPerut

class NyeriPerutForm(forms.ModelForm):
    class Meta:
        model = NyeriPerut
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(NyeriPerutForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'