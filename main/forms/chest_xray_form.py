from django import forms

from ..models.chest_xray import ChestXray

class ChestXrayForm(forms.ModelForm):
    class Meta:
        model = ChestXray
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ChestXrayForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'