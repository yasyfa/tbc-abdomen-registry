from django import forms

from ..models.analisis_cairan_pleura import AnalisisCairanPleura

class AnalisisCairanPleuraForm(forms.ModelForm):
    class Meta:
        model = AnalisisCairanPleura
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(AnalisisCairanPleuraForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'