from django import forms

from ..models.perut_membesar import PerutMembesar

class PerutMembesarForm(forms.ModelForm):
    class Meta:
        model = PerutMembesar
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PerutMembesarForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'