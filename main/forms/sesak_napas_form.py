from django import forms

from ..models.sesak_napas import SesakNapas

class SesakNapasForm(forms.ModelForm):
    class Meta:
        model = SesakNapas
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SesakNapasForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'