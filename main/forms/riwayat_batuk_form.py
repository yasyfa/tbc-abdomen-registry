from django import forms

from ..models.riwayat_batuk import RiwayatBatuk

class RiwayatBatukForm(forms.ModelForm):
    class Meta:
        model = RiwayatBatuk
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RiwayatBatukForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'