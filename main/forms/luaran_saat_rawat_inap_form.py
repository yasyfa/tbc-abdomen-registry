from django import forms

from ..models.luaran_saat_rawat_inap import LuaranSaatRawatInap

class LuaranSaatRawatInapForm(forms.ModelForm):
    class Meta:
        model = LuaranSaatRawatInap
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(LuaranSaatRawatInapForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'