from django import forms

from ..models.status_gizi import StatusGizi

class StatusGiziForm(forms.ModelForm):
    class Meta:
        model = StatusGizi
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(StatusGiziForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'