from django import forms

from ..models.analisis_cairan_asites import AnalisisCairanAsites

class AnalisisCairanAsitesForm(forms.ModelForm):
    class Meta:
        model = AnalisisCairanAsites
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AnalisisCairanAsitesForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'