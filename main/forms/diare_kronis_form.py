from django import forms

from ..models.diare_kronis import DiareKronis

class DiareKronisForm(forms.ModelForm):
    class Meta:
        model = DiareKronis
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DiareKronisForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'