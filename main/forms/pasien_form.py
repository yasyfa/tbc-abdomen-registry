from django import forms

from ..models.pasien import Pasien


class ExcelUploadForm(forms.Form):
    excel_file = forms.FileField()

class DateInput(forms.DateInput):
    input_type = 'date'


class CreatePasienForm(forms.ModelForm):
    
    class Meta:
        model = Pasien
        exclude = [
            'analisis_cairan_asites',
            'analisis_cairan_pleura',
            'bta_sputum_aspirat_lambung',
            'chest_xray',
            'diare_kronis',
            'domisili',
            'laboratorium',
            'luaran_saat_rawat_inap' ,
            'nyeri_perut',
            'pembesaran_kgb' ,
            'pemeriksaan_abdomen',
            'pemeriksaan_toraks',
            'perut_membesar',
            'riwayat_batuk',
            'riwayat_panas',
            'sesak_napas',
            'status_gizi',
            'suhu',
            'tcm_pleura',
            'usg_abdomen',
            'usia'
        ]
        labels = {
            'rm' : 'RM',
            'tanggal_perawatan' : 'Tgl Perawatan',
            'no_hp' : 'No. HP',
            'nama_pasien' : 'Nama Pasien',
            'jenis_kelamin' : 'Jenis Kelamin',
            'rujukan_dari' : 'Rujukan dari',
            'pendidikan' : 'Pendidikan',
            'status_hiv' : 'Status HIV',
            'keringat_malam' : 'Keringat malam',
            'edema' : 'Edema',
            'muntah' : 'Muntah',
            'anoreksia' : 'anoreksia',
            'hematoskezia' : 'Hematoskezia/bleed per rektum',
            'riwayat_bb_sulit_naik' : 'Riwayat BB sulit naik',
            'penurunan_bb' : 'Penurunan BB',
            'malaise' : 'Malaise',
            'riwayat_kontak_tb_dewasa' : 'Riwayat kontak TB dewasa',
            'siapa' : 'Siapa',
            'riwayat_imunisasi_bcg' : 'Riwayat imunisasi BCG',
            'riwayat_pengobatan_tb' : 'Riwayat pengobatan TBC',
            'kapan_pengobatan_tb_sebelumnya' : 'Kapan pengobatan TB sebelumnya',
            'durasi_pengobatan_tb_sebelumnya' : 'Durasi Pengobatan TB sebelumnya',
            'keadaan_umum' : 'Keadaan Umum',
            'efusi_pleura' : 'Efusi Pleura',
            'efusi_perikardium' : 'Efusi Perikardium',
            'tst' : 'TST',
            'tst_mm' : 'TST (mm)',
            'tcm_sputum_aspirat_lambung' : 'TCM Sputum/ Aspirat Lambung',
            'tcm_asites' : 'TCM Asites',
            'tcm_feses' : 'TCM Feses',
            'kultur_mtb' : 'Kultur M.Tb',
            'lcs' : 'LCS',
            'fnab_pa_jaringan' : 'FNAB/ PA jaringan',
            'ct_scan_abdomen' : 'CT Scan Abdomen',
            'diagnosis_akhir' : 'Diagnosis Akhir',
            'diagnosis_komorbiditas' : 'Diagnosis Komorbiditas',
            'regimen_oat' : 'Regimen OAT',
            'prednison' : 'Prednison',
            'efek_samping_oat' : 'Efek Samping OAT yang muncul',
            'regimen_oat_diganti_dengan' : 'Regimen OAT diganti dengan',
            'los' : 'LOS (Hari)',
            'luaran_saat_rawat_inap' : 'Luaran Saat Rawat Inap',
            'keterangan_lain' : 'Keterangan lain',
            'dpjp_utama' : 'DPJP Utama',
            'tanggal_mulai_oat' : 'Tanggal Mulai OAT',
            'status_gizi_1' : 'Status gizi (bulan ke-1)',
            'keluhan_1' : 'Keluhan (bulan ke-1)',
            'pemeriksaan_fisik_1' : 'Pemeriksaan Fisik (bulan ke-1)',
            'lab_1' : 'Lab (bulan ke-1)',
            'radiologi_1' : 'Radiologi (bulan ke-1)',
            'regimen_oat_1' : 'Regimen OAT (bulan ke-1)',
            'diagnosis_1' : 'Diagnosis (bulan ke-1)',
            'komplikasi_1' : 'Komplikasi (bulan ke-1)',
            'lain_lain_1' : 'Lain-lain (bulan ke-1)',

            'status_gizi_2' : 'Status gizi (bulan ke-2)',
            'keluhan_2' : 'Keluhan (bulan ke-2)',
            'pemeriksaan_fisik_2' : 'Pemeriksaan Fisik (bulan ke-2)',
            'lab_2' : 'Lab (bulan ke-2)',
            'radiologi_2' : 'Radiologi (bulan ke-2)',
            'regimen_oat_2' : 'Regimen OAT (bulan ke-2)',
            'diagnosis_2' : 'Diagnosis (bulan ke-2)',
            'komplikasi_2' : 'Komplikasi (bulan ke-2)',
            'lain_lain_2' : 'Lain-lain (bulan ke-2)',

            'status_gizi_3' : 'Status gizi (bulan ke-3)',
            'keluhan_3' : 'Keluhan (bulan ke-3)',
            'pemeriksaan_fisik_3' : 'Pemeriksaan Fisik (bulan ke-3)',
            'lab_3' : 'Lab (bulan ke-3)',
            'radiologi_3' : 'Radiologi (bulan ke-3)',
            'regimen_oat_3' : 'Regimen OAT (bulan ke-3)',
            'diagnosis_3' : 'Diagnosis (bulan ke-3)',
            'komplikasi_3' : 'Komplikasi (bulan ke-3)',
            'lain_lain_3' : 'Lain-lain (bulan ke-3)',

            'status_gizi_4' : 'Status gizi (bulan ke-4)',
            'keluhan_4' : 'Keluhan (bulan ke-4)',
            'pemeriksaan_fisik_4' : 'Pemeriksaan Fisik (bulan ke-4)',
            'lab_4' : 'Lab (bulan ke-4)',
            'radiologi_4' : 'Radiologi (bulan ke-4)',
            'regimen_oat_4' : 'Regimen OAT (bulan ke-4)',
            'diagnosis_4' : 'Diagnosis (bulan ke-4)',
            'komplikasi_4' : 'Komplikasi (bulan ke-4)',
            'lain_lain_4' : 'Lain-lain (bulan ke-4)',

            'status_gizi_5' : 'Status gizi (bulan ke-5)',
            'keluhan_5' : 'Keluhan (bulan ke-5)',
            'pemeriksaan_fisik_5' : 'Pemeriksaan Fisik (bulan ke-5)',
            'lab_5' : 'Lab (bulan ke-5)',
            'radiologi_5' : 'Radiologi (bulan ke-5)',
            'regimen_oat_5' : 'Regimen OAT (bulan ke-5)',
            'diagnosis_5' : 'Diagnosis (bulan ke-5)',
            'komplikasi_5' : 'Komplikasi (bulan ke-5)',
            'lain_lain_5' : 'Lain-lain (bulan ke-5)',

            'status_gizi_6' : 'Status gizi (bulan ke-6)',
            'keluhan_6' : 'Keluhan (bulan ke-6)',
            'pemeriksaan_fisik_6' : 'Pemeriksaan Fisik (bulan ke-6)',
            'lab_6' : 'Lab (bulan ke-6)',
            'radiologi_6' : 'Radiologi (bulan ke-6)',
            'regimen_oat_6' : 'Regimen OAT (bulan ke-6)',
            'diagnosis_6' : 'Diagnosis (bulan ke-6)',
            'komplikasi_6' : 'Komplikasi (bulan ke-6)',
            'lain_lain_6' : 'Lain-lain (bulan ke-6)',

            'status_gizi_7' : 'Status gizi (bulan ke-7)',
            'keluhan_7' : 'Keluhan (bulan ke-7)',
            'pemeriksaan_fisik_7' : 'Pemeriksaan Fisik (bulan ke-7)',
            'lab_7' : 'Lab (bulan ke-7)',
            'radiologi_7' : 'Radiologi (bulan ke-7)',
            'regimen_oat_7' : 'Regimen OAT (bulan ke-7)',
            'diagnosis_7' : 'Diagnosis (bulan ke-7)',
            'komplikasi_7' : 'Komplikasi (bulan ke-7)',
            'lain_lain_7' : 'Lain-lain (bulan ke-7)',

            'status_gizi_8' : 'Status gizi (bulan ke-8)',
            'keluhan_8' : 'Keluhan (bulan ke-8)',
            'pemeriksaan_fisik_8' : 'Pemeriksaan Fisik (bulan ke-8)',
            'lab_8' : 'Lab (bulan ke-8)',
            'radiologi_8' : 'Radiologi (bulan ke-8)',
            'regimen_oat_8' : 'Regimen OAT (bulan ke-8)',
            'diagnosis_8' : 'Diagnosis (bulan ke-8)',
            'komplikasi_8' : 'Komplikasi (bulan ke-8)',
            'lain_lain_8' : 'Lain-lain (bulan ke-8)',

            'status_gizi_9' : 'Status gizi (bulan ke-9)',
            'keluhan_9' : 'Keluhan (bulan ke-9)',
            'pemeriksaan_fisik_9' : 'Pemeriksaan Fisik (bulan ke-9)',
            'lab_9' : 'Lab (bulan ke-9)',
            'radiologi_9' : 'Radiologi (bulan ke-9)',
            'regimen_oat_9' : 'Regimen OAT (bulan ke-9)',
            'diagnosis_9' : 'Diagnosis (bulan ke-9)',
            'komplikasi_9' : 'Komplikasi (bulan ke-9)',
            'lain_lain_9' : 'Lain-lain (bulan ke-9)',

            'status_gizi_10' : 'Status gizi (bulan ke-10)',
            'keluhan_10' : 'Keluhan (bulan ke-10)',
            'pemeriksaan_fisik_10' : 'Pemeriksaan Fisik (bulan ke-10)',
            'lab_10' : 'Lab (bulan ke-10)',
            'radiologi_10' : 'Radiologi (bulan ke-10)',
            'regimen_oat_10' : 'Regimen OAT (bulan ke-10)',
            'diagnosis_10' : 'Diagnosis (bulan ke-10)',
            'komplikasi_10' : 'Komplikasi (bulan ke-10)',
            'lain_lain_10' : 'Lain-lain (bulan ke-10)',

            'status_gizi_11' : 'Status gizi (bulan ke-11)',
            'keluhan_11' : 'Keluhan (bulan ke-11)',
            'pemeriksaan_fisik_11' : 'Pemeriksaan Fisik (bulan ke-11)',
            'lab_11' : 'Lab (bulan ke-11)',
            'radiologi_11' : 'Radiologi (bulan ke-11)',
            'regimen_oat_11' : 'Regimen OAT (bulan ke-11)',
            'diagnosis_11' : 'Diagnosis (bulan ke-11)',
            'komplikasi_11' : 'Komplikasi (bulan ke-11)',
            'lain_lain_11' : 'Lain-lain (bulan ke-11)',

            'status_gizi_12' : 'Status gizi (bulan ke-12)',
            'keluhan_12' : 'Keluhan (bulan ke-12)',
            'pemeriksaan_fisik_12' : 'Pemeriksaan Fisik (bulan ke-12)',
            'lab_12' : 'Lab (bulan ke-12)',
            'radiologi_12' : 'Radiologi (bulan ke-12)',
            'regimen_oat_12' : 'Regimen OAT (bulan ke-12)',
            'diagnosis_12' : 'Diagnosis (bulan ke-12)',
            'komplikasi_12' : 'Komplikasi (bulan ke-12)',
            'lain_lain_12' : 'Lain-lain (bulan ke-12)',

            'durasi_oat' : 'Durasi OAT',
            'hasil_akhir_pengobatan' : 'Hasil Akhir Pengobatan'



        }
        widgets = { 
            "tanggal_perawatan" : DateInput(
                attrs={
                    'class': 'form-control form-section'
                }),
            "tanggal_mulai_oat" : DateInput(
                attrs={
                    'class': 'form-control form-section'
                }),
 
            }

    def __init__(self, *args, **kwargs):
        super(CreatePasienForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'




