from django import forms

from ..models.pembesaran_kgb import PembesaranKgb

class PembesaranKgbForm(forms.ModelForm):
    class Meta:
        model = PembesaranKgb
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PembesaranKgbForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'