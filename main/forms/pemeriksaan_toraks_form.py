from django import forms

from ..models.pemeriksaan_toraks import PemeriksaanToraks

class PemeriksaanToraksForm(forms.ModelForm):
    class Meta:
        model = PemeriksaanToraks
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(PemeriksaanToraksForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'