from django import forms

from ..models.tcm_pleura import TcmPleura

class TcmPleuraForm(forms.ModelForm):
    class Meta:
        model = TcmPleura
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TcmPleuraForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'