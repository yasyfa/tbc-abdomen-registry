from .models.pasien import Pasien
from .models.analisis_cairan_asites import AnalisisCairanAsites
from .models.analisis_cairan_pleura import AnalisisCairanPleura
from .models.bta_sputum_aspirat_lambung import BtaSputumAspiratLambung
from .models.chest_xray import ChestXray
from .models.diare_kronis import DiareKronis
from .models.domisili import Domisili
from .models.laboratorium import Laboratorium
from .models.luaran_saat_rawat_inap import LuaranSaatRawatInap
from .models.nyeri_perut import NyeriPerut
from .models.pembesaran_kgb import PembesaranKgb
from .models.pemeriksaan_abdomen import PemeriksaanAbdomen
from .models.pemeriksaan_toraks import PemeriksaanToraks
from .models.perut_membesar import PerutMembesar
from .models.riwayat_batuk import RiwayatBatuk
from .models.riwayat_panas import RiwayatPanas
from .models.sesak_napas import SesakNapas
from .models.status_gizi import StatusGizi
from .models.suhu import Suhu
from .models.tcm_pleura import TcmPleura
from .models.usg_abdomen import UsgAbdomen
from .models.usia import Usia

def get_all_data():
    pasien = Pasien.objects.all()
    analisis_cairan_asites = AnalisisCairanAsites.objects.all()
    analisis_cairan_pleura = AnalisisCairanPleura.objects.all()
    bta_sputum_aspirat_lambung = BtaSputumAspiratLambung.objects.all()
    chest_xray = ChestXray.objects.all()
    diare_kronis = DiareKronis.objects.all()
    domisili = Domisili.objects.all()
    laboratorium = Laboratorium.objects.all()
    luaran_saat_rawat_inap = LuaranSaatRawatInap.objects.all()
    nyeri_perut = NyeriPerut.objects.all()
    pembesaran_kgb = PembesaranKgb.objects.all()
    pemeriksaan_abdomen = PemeriksaanAbdomen.objects.all()
    pemeriksaan_toraks = PemeriksaanToraks.objects.all()
    perut_membesar = PerutMembesar.objects.all()
    riwayat_batuk = RiwayatBatuk.objects.all()
    riwayat_panas = RiwayatPanas.objects.all()
    sesak_napas = SesakNapas.objects.all()
    status_gizi = StatusGizi.objects.all()
    suhu = Suhu.objects.all()
    usia = Usia.objects.all()
    tcm_pleura = TcmPleura.objects.all()
    usg_abdomen = UsgAbdomen.objects.all()

    # all_set = pasien | analisis_cairan_asites | analisis_cairan_pleura |analisis_cairan_pleura

    context = {
        'pasien' : pasien,
        'usia' : usia,
        'domisili' : domisili,
        'status_gizi' : status_gizi,
        'perut_membesar' : perut_membesar,
        'nyeri_perut' : nyeri_perut,
        'chest_xray' : chest_xray,
        'analisis_cairan_asites' : analisis_cairan_asites,
        'analisis_cairan_pleura' : analisis_cairan_pleura,
        'suhu' : suhu,
        'tcm_pleura' : tcm_pleura,
        'sesak_napas' : sesak_napas,
        'usg_abdomen' : usg_abdomen,
        'diare_kronis' : diare_kronis,
        'laboratorium' : laboratorium,
        'riwayat_batuk' : riwayat_batuk,
        'riwayat_panas' : riwayat_panas,
        'pembesaran_kgb' : pembesaran_kgb,
        'pemeriksaan_toraks' : pemeriksaan_toraks,
        'luaran_saat_rawat_inap' : luaran_saat_rawat_inap,
        'pemeriksaan_abdomen' : pemeriksaan_abdomen,
        'bta_sputum_aspirat_lambung': bta_sputum_aspirat_lambung,
    }
    
    return context

def getHeader():
    header = ["L", "P","Bandung","Luar Bandung","Tidak Bersekolah","SD","SMP","SMA",
        "Usia","<5 Tahun","5-9 Tahun","10-14 Tahun", "15-17 Tahun", 
        "Normal", "Malnutrisi Sedang", "Malnutrisi Berat","BB(kg)","TB(cm)","LLA","TB/U","BMI/U","BB/TB","LLA/U",
        "Positf","Negatif","Tidak Diketahui",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak",
        "Febris","Suhu",
        "Ya","Tidak","Lokasi",
        "Normal","abnormal","Deskripsi",
        "Ya","Tidak",
        "Ya","Tidak",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak","Keterangan",
        "Ya","Tidak",
        "Ya","Tidak",
        "HB","HT","Leukosit","Trombosit","Basofil","Eosinofil","N. Batang","N. Segemen","Limfosit","Monosit","SGOT","SGPT","Bil. Total", "Bil. Direk", "Albumin", "CRP",
        "Dilakukan","Tidak Dilakukan",
        "Pembesaran KGB", "Asites", "Hepatosplenomegali", "Lain-lain", "Ekspertise",
        "Normal", "Kalsifikasi dengan infiltat","Pembesaran KGB Hilus atau paratrakeal","Kavitas","Efusi Pleura","Konsolidasi","Milier","Tuberkuloma","Atelektasi","Ekspertise",
        "Positif","Negatif","Positif","Negatif","Positif","Negatif",
        "MTB Detected", "MTB Not Detected",
        "MTB Detected", "MTB Not Detected",
        "MTB Detected", "MTB Not Detected", "Keterangan",
        "MTB Detected", "MTB Not Detected",
        "Warna", "Rivalta", "Jumlah Sel", "PMN", "MN" , "Glukosa" , "Albumin" ,"Protein" , "LDH",
        "Warna", "Rivalta", "Jumlah Sel", "PMN", "MN" , "Glukosa" , "Protein" ,"Albumin" , "LDH",
        "Ya","Tidak",
        "Pulang Paksa", "Perbaikan", "Meninggal"]
        
    for i in range(0,12):
        header.extend(["Status gizi", "Keluhan", "Pemeriksaan Fisik", "Lab","Radiologi", "Regimen OAT", "Diagnosis", "Komplikasi", "Lain-lain"])
    return header
